# ClusterDef

`ClusterDef` is a custom resource describing a git source for a cluster in the Kanod LCM.
Documentation: https://orange-opensource.gitlab.io/kanod/reference/clusterdef/index.html