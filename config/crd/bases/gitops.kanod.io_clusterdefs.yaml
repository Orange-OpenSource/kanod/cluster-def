
---
apiVersion: apiextensions.k8s.io/v1
kind: CustomResourceDefinition
metadata:
  annotations:
    controller-gen.kubebuilder.io/version: v0.4.1
  creationTimestamp: null
  name: clusterdefs.gitops.kanod.io
spec:
  group: gitops.kanod.io
  names:
    kind: ClusterDef
    listKind: ClusterDefList
    plural: clusterdefs
    singular: clusterdef
  scope: Namespaced
  versions:
  - additionalPrinterColumns:
    - jsonPath: .status.phase
      name: Phase
      type: string
    - jsonPath: .spec.source.repository
      name: Repository
      type: string
    - jsonPath: .spec.source.branch
      name: Branch
      priority: 10
      type: string
    - jsonPath: .status.pivotstatus
      name: PivotStatus
      type: string
    name: v1alpha1
    schema:
      openAPIV3Schema:
        properties:
          apiVersion:
            description: 'APIVersion defines the versioned schema of this representation
              of an object. Servers should convert recognized schemas to the latest
              internal value, and may reject unrecognized values. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#resources'
            type: string
          kind:
            description: 'Kind is a string value representing the REST resource this
              object represents. Servers may infer this from the endpoint the client
              submits requests to. Cannot be updated. In CamelCase. More info: https://git.k8s.io/community/contributors/devel/sig-architecture/api-conventions.md#types-kinds'
            type: string
          metadata:
            type: object
          spec:
            description: ClusterDefSpec defines the desired state of ClusterDef
            properties:
              cidataName:
                description: CidataName is the name of the configmap that customize
                  the cluster cloud-init datas. This has precedence over all other
                  sources of cloud-init data.
                type: string
              configurationName:
                description: ConfigurationName is the configuration supplied by the
                  infrastructure administrator to render the specification of the
                  cluster definition It is a configmap in the same namespace
                type: string
              createArgocdClusterSecret:
                description: CreateArgoCDSecret indicates if the argoCD secret associated
                  with the target cluster should be created
                type: boolean
              credentialName:
                description: CredentialName is the user credentials for accessing
                  git repository It must be in a secret in the same namespace
                type: string
              hostQuotaList:
                description: HostQuotaList contains a list of HostQuota resource
                items:
                  type: string
                type: array
              multiTenant:
                description: 'MultiTenant is a boolean that indicates if the cluster
                  namespace is managed in multitenant mode. If MultiTenant = false
                  : the target cluster namespace is the same as the clusterdef resource.
                  If MultiTenant = true : the target cluster namespace is the concatenation
                  of name/namespace. of the clusterdef resource'
                type: boolean
              multusNetworks:
                description: MultusNetworks are the Multus networks associated to
                  the cluster
                items:
                  description: MultusNetwork describes a multus network (bridge kind)
                    to connect
                  properties:
                    bridge:
                      description: Bridge is the name of the Linux bridge supporting
                        the network
                      type: string
                    name:
                      description: Name is the name of the multus network as used
                        in cluster definition
                      type: string
                    vlan:
                      description: Vlan is an optional vlanid
                      type: integer
                  required:
                  - bridge
                  - name
                  type: object
                type: array
              networkList:
                additionalProperties:
                  description: NetworkSpec contains information for interacting with
                    a network definition
                  properties:
                    address:
                      description: Address is the address of the brokernet
                      type: string
                    brokerConfig:
                      description: BrokerConfig is the name of the configmap containing
                        the brokernet CA
                      type: string
                    brokerCredentials:
                      description: BrokerCredentials is the name of the secret containing
                        the credentials for the brokernet
                      type: string
                    networkdefName:
                      description: NetworkdefName is the name of the network definition
                      type: string
                  required:
                  - address
                  - brokerConfig
                  - brokerCredentials
                  - networkdefName
                  type: object
                description: NetworkList contains information to access the brokernet
                  for a network definition
                type: object
              pivotInfo:
                description: PivotInfo contains information needed for the pivoting
                  of the management functions from the management cluster to the workload
                  cluster.
                properties:
                  bareMetalPoolList:
                    description: BareMetalPoolList contains the list of BareMetalPool
                      custom resources to be moved on the target cluster
                    items:
                      type: string
                    type: array
                  byoHostPoolUsed:
                    description: ByoHostPoolUsed indicates id byohostpool is used
                    type: boolean
                  dhcpConfigList:
                    description: DhcpConfigList contains the list of DhcpConfig custom
                      resources
                    items:
                      type: string
                    type: array
                  ippoolList:
                    description: IppoolList contains the list of Ippool custom resources
                    items:
                      type: string
                    type: array
                  ironicItf:
                    description: interface used by ironic
                    type: string
                  kanodName:
                    description: Kanod operator custom resource name
                    type: string
                  kanodNamespace:
                    description: namespace of the Kanod operator custom resource
                    type: string
                  networkList:
                    description: NetworkList contains the list of Network custom resources
                      to be moved on the target cluster
                    items:
                      type: string
                    type: array
                  pivot:
                    description: Pivot indicates if the cluster pivoting process should
                      be launched
                    type: boolean
                required:
                - ironicItf
                - kanodName
                - kanodNamespace
                - pivot
                type: object
              poolUserList:
                additionalProperties:
                  description: PoolUser contains information for interacting with
                    a poolUser
                  properties:
                    address:
                      description: Address is the address of the broker
                      type: string
                    brokerConfig:
                      description: BrokerConfig is the name of the configmap containing
                        the broker CA
                      type: string
                    brokerCredentials:
                      description: BrokerCredentials is the name of the secret containing
                        the credentials for the broker
                      type: string
                    username:
                      description: Username of the pool user
                      type: string
                  required:
                  - address
                  - brokerConfig
                  - brokerCredentials
                  - username
                  type: object
                description: PoolUserList contains information to access the brokerdef
                  for a pool user
                type: object
              source:
                description: Source is the location of the cluster definition.
                properties:
                  branch:
                    description: Branch is the git branch followed by ClusterDef
                    type: string
                  path:
                    description: Path is the subpath of the folder hosting the configuration
                      file.
                    type: string
                  repository:
                    description: Repository is the URL of the project hosting the
                      files
                    type: string
                required:
                - repository
                type: object
            required:
            - configurationName
            - credentialName
            - source
            type: object
          status:
            description: ClusterDefStatus defines the observed state of ClusterDef
            properties:
              controlPlaneVersion:
                description: ControlPlaneVersion is the version supported by control
                  plane
                type: string
              machineVersions:
                description: MachineVersions is a sorted array of different versions
                  of the kubelet deployed on machines
                items:
                  type: string
                type: array
              phase:
                description: Status of deployment
                enum:
                - ApplicationCreated
                - ApplicationSynced
                - ClusterProvisionned
                - Ready
                - Running
                - Failed
                type: string
              pivotstatus:
                description: Status of pivot
                enum:
                - NotPivoted
                - TargetClusterInfoStored
                - KanodOperatorInstalled
                - KanodOperatorReady
                - KanodResourcesMoved
                - StackDeployed
                - ArgoAppDeleted
                - ResourcesLabeled
                - BareMetalPoolPaused
                - ByoHostPoolPaused
                - ByoHostPausedAnConfigured
                - NetworkPaused
                - ResourcesPivoted
                - ResourcesUnlabeled
                - BmpResourcesPivoted
                - ByoHostPoolResourcesPivoted
                - NetworkResourcesPivoted
                - BareMetalPoolUnPaused
                - ByoHostUnPaused
                - ByoHostPoolUnPaused
                - NetworkUnPaused
                - IronicOnLocalClusterScaledToZero
                - IronicOnLocalClusterUndeployed
                - IronicOnLocalClusterScaledToOne
                - IronicOnLocalClusterRedeployed
                - ClusterPivoted
                type: string
            type: object
        type: object
    served: true
    storage: true
    subresources:
      status: {}
status:
  acceptedNames:
    kind: ""
    plural: ""
  conditions: []
  storedVersions: []
