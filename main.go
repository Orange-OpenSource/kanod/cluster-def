/*
Copyright 2020-2022 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"flag"
	"os"

	"k8s.io/apimachinery/pkg/runtime"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	_ "k8s.io/client-go/plugin/pkg/client/auth/gcp"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/log/zap"

	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	"gitlab.com/Orange-OpenSource/kanod/cluster-def/controllers"

	// +kubebuilder:scaffold:imports
	kcpkamajiv1alpha1 "github.com/clastix/cluster-api-control-plane-provider-kamaji/api/v1alpha1"
	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	byohv1beta1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/byoh_api/v1beta1"
	argocdv1alpha1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/thirdparty/argocd/apis/v1alpha1"

	m3ipam "github.com/metal3-io/ip-address-manager/api/v1alpha1"
	hostquota "gitlab.com/Orange-OpenSource/kanod/host-quota/api/v1alpha1"
	keav1alpha1 "gitlab.com/Orange-OpenSource/kanod/kanod-kea/api/v1alpha1"
	capiv1beta1 "sigs.k8s.io/cluster-api/api/v1beta1"
	controlplanev1beta1 "sigs.k8s.io/cluster-api/controlplane/kubeadm/api/v1beta1"
	metricsserver "sigs.k8s.io/controller-runtime/pkg/metrics/server"
)

var (
	scheme   = runtime.NewScheme()
	setupLog = ctrl.Log.WithName("setup")
)

func init() {
	_ = clientgoscheme.AddToScheme(scheme)

	_ = gitopsv1.AddToScheme(scheme)
	// +kubebuilder:scaffold:scheme
	_ = argocdv1alpha1.AddToScheme(scheme)
	_ = capiv1beta1.AddToScheme(scheme)
	_ = controlplanev1beta1.AddToScheme(scheme)
	_ = kcpkamajiv1alpha1.AddToScheme(scheme)
	_ = bmhv1alpha1.AddToScheme(scheme)
	_ = byohv1beta1.AddToScheme(scheme)
	_ = keav1alpha1.AddToScheme(scheme)
	_ = m3ipam.AddToScheme(scheme)
	_ = hostquota.AddToScheme(scheme)

}

func main() {
	var metricsAddr string
	var enableLeaderElection bool
	flag.StringVar(&metricsAddr, "metrics-addr", ":8080", "The address the metric endpoint binds to.")
	flag.BoolVar(&enableLeaderElection, "enable-leader-election", false,
		"Enable leader election for controller manager. "+
			"Enabling this will ensure there is only one active controller manager.")
	opts := zap.Options{
		Development: true,
	}
	opts.BindFlags(flag.CommandLine)
	flag.Parse()

	ctrl.SetLogger(zap.New(zap.UseFlagOptions(&opts)))

	mgr, err := ctrl.NewManager(ctrl.GetConfigOrDie(), ctrl.Options{
		Scheme:           scheme,
		Metrics:          metricsserver.Options{BindAddress: metricsAddr},
		LeaderElection:   enableLeaderElection,
		LeaderElectionID: "dc322f55.kanod.io",
	})
	if err != nil {
		setupLog.Error(err, "unable to start manager")
		os.Exit(1)
	}

	if err = (&controllers.ClusterDefReconciler{
		Client: mgr.GetClient(),
		Log:    ctrl.Log.WithName("controllers").WithName("ClusterDef"),
		Scheme: mgr.GetScheme(),
		Config: controllers.MakeConfig(),
		State:  make(map[string]*controllers.ClusterDefStateCache),
	}).SetupWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create controller", "controller", "ClusterDef")
		os.Exit(1)
	}
	if err = (&gitopsv1.ClusterDef{}).SetupWebhookWithManager(mgr); err != nil {
		setupLog.Error(err, "unable to create webhook", "webhook", "ClusterDef")
		os.Exit(1)
	}
	// +kubebuilder:scaffold:builder

	setupLog.Info("starting manager")
	if err := mgr.Start(ctrl.SetupSignalHandler()); err != nil {
		setupLog.Error(err, "problem running manager")
		os.Exit(1)
	}
}
