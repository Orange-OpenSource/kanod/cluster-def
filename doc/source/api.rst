API
===

Specification
-------------
.. jsonschema:: schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/spec
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target:

Status
------
.. jsonschema:: schema.yaml#/spec/versions/0/schema/openAPIV3Schema/properties/status
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target: