Cluster-Def parameters
======================
Controller Environment Variables
--------------------------------

ARGOCD_NAMESPACE
    Namespace of the argocd applications (default is argocd)

INFRA_NAMESPACE
    Namespace where the infra must be defined (default is cluster-def)

ARGOCD_PLUGIN_NAME
    Name of the plugin used to create cluster-api manifests from kanod cluster
    description

Snapshot credentials
--------------------

There are two options for the authentication on the git server:

* For HTTPS URL, the secret must contains two fields: ``username`` and
  ``password``.
* For git ssh URL, the secret must contain an ``ssh`` field. The value
  of the field is the *private* ssh key  necessary to access the repository.
