module gitlab.com/Orange-OpenSource/kanod/cluster-def

go 1.20

replace github.com/clastix/cluster-api-control-plane-provider-kamaji => github.com/clastix/cluster-api-control-plane-provider-kamaji v0.2.0

require (
	github.com/clastix/cluster-api-control-plane-provider-kamaji v0.3.0
	github.com/drone/envsubst v1.0.3
	github.com/ghodss/yaml v1.0.0
	github.com/go-logr/logr v1.4.1
	github.com/metal3-io/baremetal-operator/apis v0.5.0
	github.com/metal3-io/ip-address-manager/api v1.6.0
	github.com/pkg/errors v0.9.1
	github.com/robfig/cron v1.2.0
	github.com/sirupsen/logrus v1.9.0
	gitlab.com/Orange-OpenSource/kanod/baremetalpool v0.2.3
	gitlab.com/Orange-OpenSource/kanod/brokerdef v0.2.4
	gitlab.com/Orange-OpenSource/kanod/host-quota v0.1.2
	gitlab.com/Orange-OpenSource/kanod/kanod-kea v0.2.1
	k8s.io/api v0.29.0
	k8s.io/apiextensions-apiserver v0.29.0
	k8s.io/apimachinery v0.29.2
	k8s.io/client-go v0.29.0
	sigs.k8s.io/cluster-api v1.6.1
	sigs.k8s.io/controller-runtime v0.17.2

)

require (
	github.com/MakeNowJust/heredoc v1.0.0 // indirect
	github.com/Masterminds/goutils v1.1.1 // indirect
	github.com/Masterminds/semver/v3 v3.2.0 // indirect
	github.com/Masterminds/sprig/v3 v3.2.3 // indirect
	github.com/ProtonMail/go-crypto v0.0.0-20230217124315-7d5c6f04bbb8 // indirect
	github.com/adrg/xdg v0.4.0 // indirect
	github.com/antlr/antlr4/runtime/Go/antlr/v4 v4.0.0-20230305170008-8188dc5388df // indirect
	github.com/apapsch/go-jsonmerge/v2 v2.0.0 // indirect
	github.com/asaskevich/govalidator v0.0.0-20190424111038-f61b66f89f4a // indirect
	github.com/beorn7/perks v1.0.1 // indirect
	github.com/blang/semver/v4 v4.0.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/clastix/kamaji v0.3.0 // indirect
	github.com/cloudflare/circl v1.3.7 // indirect
	github.com/coredns/caddy v1.1.1 // indirect
	github.com/davecgh/go-spew v1.1.2-0.20180830191138-d8f796af33cc // indirect
	github.com/distribution/reference v0.5.0 // indirect
	github.com/drone/envsubst/v2 v2.0.0-20210730161058-179042472c46 // indirect
	github.com/emicklei/go-restful/v3 v3.11.0 // indirect
	github.com/evanphx/json-patch v5.6.0+incompatible // indirect
	github.com/evanphx/json-patch/v5 v5.8.0 // indirect
	github.com/fsnotify/fsnotify v1.7.0 // indirect
	github.com/getkin/kin-openapi v0.76.0 // indirect
	github.com/go-logr/zapr v1.3.0 // indirect
	github.com/go-openapi/jsonpointer v0.19.6 // indirect
	github.com/go-openapi/jsonreference v0.20.2 // indirect
	github.com/go-openapi/swag v0.22.3 // indirect
	github.com/gobuffalo/flect v1.0.2 // indirect
	github.com/gogo/protobuf v1.3.2 // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/cel-go v0.17.7 // indirect
	github.com/google/gnostic-models v0.6.8 // indirect
	github.com/google/go-cmp v0.6.0 // indirect
	github.com/google/go-github/v53 v53.2.0 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/google/gofuzz v1.2.0 // indirect
	github.com/google/uuid v1.5.0 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/hashicorp/hcl v1.0.0 // indirect
	github.com/huandu/xstrings v1.3.3 // indirect
	github.com/imdario/mergo v0.3.13 // indirect
	github.com/josharian/intern v1.0.0 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/magiconair/properties v1.8.7 // indirect
	github.com/mailru/easyjson v0.7.7 // indirect
	github.com/matttproud/golang_protobuf_extensions/v2 v2.0.0 // indirect
	github.com/metal3-io/baremetal-operator/pkg/hardwareutils v0.4.0 // indirect
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/munnerz/goautoneg v0.0.0-20191010083416-a7dc8b61c822 // indirect
	github.com/oapi-codegen/runtime v1.0.0 // indirect
	github.com/onsi/gomega v1.30.0 // indirect
	github.com/opencontainers/go-digest v1.0.0 // indirect
	github.com/pelletier/go-toml/v2 v2.1.0 // indirect
	github.com/prometheus/client_golang v1.18.0 // indirect
	github.com/prometheus/client_model v0.5.0 // indirect
	github.com/prometheus/common v0.45.0 // indirect
	github.com/prometheus/procfs v0.12.0 // indirect
	github.com/rogpeppe/go-internal v1.11.0 // indirect
	github.com/sagikazarmark/locafero v0.3.0 // indirect
	github.com/sagikazarmark/slog-shim v0.1.0 // indirect
	github.com/shopspring/decimal v1.3.1 // indirect
	github.com/sourcegraph/conc v0.3.0 // indirect
	github.com/spf13/afero v1.10.0 // indirect
	github.com/spf13/cast v1.5.1 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/spf13/viper v1.17.0 // indirect
	github.com/stoewer/go-strcase v1.2.0 // indirect
	github.com/subosito/gotenv v1.6.0 // indirect
	github.com/valyala/fastjson v1.6.4 // indirect
	gitlab.com/Orange-OpenSource/kanod/brokerdef/api v0.2.3 // indirect
	go.uber.org/multierr v1.11.0 // indirect
	go.uber.org/zap v1.26.0 // indirect
	golang.org/x/crypto v0.17.0 // indirect
	golang.org/x/exp v0.0.0-20230905200255-921286631fa9 // indirect
	golang.org/x/net v0.19.0 // indirect
	golang.org/x/oauth2 v0.14.0 // indirect
	golang.org/x/sync v0.5.0 // indirect
	golang.org/x/sys v0.16.0 // indirect
	golang.org/x/term v0.15.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/time v0.3.0 // indirect
	gomodules.xyz/jsonpatch/v2 v2.4.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto/googleapis/api v0.0.0-20230913181813-007df8e322eb // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20230920204549-e6e6cdab5c13 // indirect
	google.golang.org/protobuf v1.31.0 // indirect
	gopkg.in/inf.v0 v0.9.1 // indirect
	gopkg.in/ini.v1 v1.67.0 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
	k8s.io/apiserver v0.29.0 // indirect
	k8s.io/cluster-bootstrap v0.28.4 // indirect
	k8s.io/component-base v0.29.0 // indirect
	k8s.io/klog/v2 v2.110.1 // indirect
	k8s.io/kube-openapi v0.0.0-20231010175941-2dd684a91f00 // indirect
	k8s.io/utils v0.0.0-20230726121419-3b25d923346b // indirect
	sigs.k8s.io/json v0.0.0-20221116044647-bc3834ca7abd // indirect
	sigs.k8s.io/structured-merge-diff/v4 v4.4.1 // indirect
	sigs.k8s.io/yaml v1.4.0 // indirect
)

replace github.com/google/cel-go => github.com/google/cel-go v0.16.1

replace k8s.io/dynamic-resource-allocation => k8s.io/dynamic-resource-allocation v0.28.4

replace k8s.io/kms => k8s.io/kms v0.28.4

replace k8s.io/api => k8s.io/api v0.28.4

replace k8s.io/apiextensions-apiserver => k8s.io/apiextensions-apiserver v0.28.4

replace k8s.io/apimachinery => k8s.io/apimachinery v0.28.4

replace k8s.io/apiserver => k8s.io/apiserver v0.28.4

replace k8s.io/cli-runtime => k8s.io/cli-runtime v0.28.4

replace k8s.io/client-go => k8s.io/client-go v0.28.4

replace k8s.io/cloud-provider => k8s.io/cloud-provider v0.28.4

replace k8s.io/cluster-bootstrap => k8s.io/cluster-bootstrap v0.28.4

replace k8s.io/code-generator => k8s.io/code-generator v0.28.4

replace k8s.io/component-base => k8s.io/component-base v0.28.4

replace k8s.io/component-helpers => k8s.io/component-helpers v0.28.4

replace k8s.io/controller-manager => k8s.io/controller-manager v0.28.4

replace k8s.io/cri-api => k8s.io/cri-api v0.28.4

replace k8s.io/csi-translation-lib => k8s.io/csi-translation-lib v0.28.4

replace k8s.io/endpointslice => k8s.io/endpointslice v0.28.4

replace k8s.io/kube-aggregator => k8s.io/kube-aggregator v0.28.4

replace k8s.io/kube-controller-manager => k8s.io/kube-controller-manager v0.28.4

replace k8s.io/kube-proxy => k8s.io/kube-proxy v0.28.4

replace k8s.io/kube-scheduler => k8s.io/kube-scheduler v0.28.4

replace k8s.io/kubectl => k8s.io/kubectl v0.28.4

replace k8s.io/kubelet => k8s.io/kubelet v0.28.4

replace k8s.io/legacy-cloud-providers => k8s.io/legacy-cloud-providers v0.28.4

replace k8s.io/metrics => k8s.io/metrics v0.28.4

replace k8s.io/mount-utils => k8s.io/mount-utils v0.28.4

replace k8s.io/pod-security-admission => k8s.io/pod-security-admission v0.28.4

replace k8s.io/sample-apiserver => k8s.io/sample-apiserver v0.28.4

replace k8s.io/sample-cli-plugin => k8s.io/sample-cli-plugin v0.28.4

replace k8s.io/sample-controller => k8s.io/sample-controller v0.28.4
