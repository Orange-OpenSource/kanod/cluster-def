/*
Copyright 2020-22 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"crypto/sha256"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"net/http"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"
	broker "gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"
	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	argocd "gitlab.com/Orange-OpenSource/kanod/cluster-def/thirdparty/argocd/apis/v1alpha1"
	hostquota "gitlab.com/Orange-OpenSource/kanod/host-quota/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

type PasswordData struct {
	Password string
}

type NetDefCredentials struct {
	Username string
	Password string
}

func (r *ClusterDefReconciler) WithHttpsClient(Certificate string) broker.ClientOption {
	return func(c *broker.Client) error {

		c.Client = r.NewHttpClient(Certificate)
		return nil
	}
}

func addBasicAuth(username string, password string) broker.RequestEditorFn {
	return func(ctx context.Context, req *http.Request) error {
		req.SetBasicAuth(string(username), string(password))
		return nil
	}
}

// makeApplication create the argocd application associated to the clusterDef
func (r *ClusterDefReconciler) makeApplication(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	destServer string,
) error {
	var name = fmt.Sprintf("cdef-%s", clusterDef.Name)
	var app argocd.Application

	targetClusterNamespace := r.getTargetClusterNamespace(clusterDef)
	app.ObjectMeta = metav1.ObjectMeta{
		Name:      name,
		Namespace: r.Config.ArgoCDNamespace}
	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &app,
		func() error {
			spec := &app.Spec
			source := &spec.Source
			dest := &spec.Destination
			source.RepoURL = clusterDef.Spec.Source.Repository
			if clusterDef.Spec.Source.Branch != "" {
				source.TargetRevision = clusterDef.Spec.Source.Branch
			}
			source.Path = "."
			if clusterDef.Spec.Source.Path != "" {
				source.Path = clusterDef.Spec.Source.Path
			}
			source.Plugin = &argocd.ApplicationSourcePlugin{
				Env: []*argocd.EnvEntry{
					{
						Name:  "INFRA_CONFIGMAP",
						Value: clusterDef.Spec.ConfigurationName,
					},
					{
						Name:  "INFRA_NAMESPACE",
						Value: r.Config.InfraNamespace,
					},
					{
						Name:  "CDEF_NAMESPACE",
						Value: clusterDef.Namespace,
					},
					{
						Name:  "TARGET_NAMESPACE",
						Value: targetClusterNamespace,
					},
					{
						Name:  "CIDATA_CONFIGMAP",
						Value: clusterDef.Spec.CidataName,
					},
					{
						Name:  "CLUSTER_NAME",
						Value: clusterDef.Name,
					},
					{
						Name:  "CLUSTER_CONFIG_FILE",
						Value: r.Config.ClusterFileName,
					},
					{
						Name:  "PLUGIN_MODE",
						Value: "updater",
					},
				},
			}
			dest.Namespace = targetClusterNamespace
			dest.Server = destServer
			spec.IgnoreDifferences = []argocd.ResourceIgnoreDifferences{
				{
					Group: "infrastructure.cluster.x-k8s.io",
					JSONPointers: []string{
						"/spec/noCloudProvider",
					},
					Kind: "Metal3Cluster",
				},
				{
					Group: "*",
					Kind:  "*",
					ManagedFieldsManagers: []string{
						"kanod-poolscaler",
					},
				},
				{
					Group: "bmp.kanod.io",
					Kind:  "BareMetalPool",
					JSONPointers: []string{
						"/metadata/annotations/baremetalpool.kanod.io~1disabled",
					},
				},
				{
					Group: "cluster.x-k8s.io",
					Kind:  "MachineHealthCheck",
					JQPathExpressions: []string{
						".spec.unhealthyConditions[].timeout",
						".spec.nodeStartupTimeout",
					},
				},
				{
					Group: "pools.kanod.io",
					Kind:  "ByoHostPoolScaler",
					JSONPointers: []string{
						"/spec/apiServer",
					},
				},
			}
			spec.Project = "default"
			spec.SyncPolicy = &argocd.SyncPolicy{
				Automated: &argocd.SyncPolicyAutomated{SelfHeal: true},
			}
			return nil
		},
	)
	return err
}

// makeCredentials create a credential secret for the repository
func (r *ClusterDefReconciler) makeCredentials(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
) error {
	// No credentials defined.
	// TODO: We should probably remove the secret if it existed
	if clusterDef.Spec.CredentialName == "" {
		return nil
	}
	name := fmt.Sprintf("cdef-%s", clusterDef.Name)
	var sec corev1.Secret
	sec.ObjectMeta = metav1.ObjectMeta{
		Name: name, Namespace: r.Config.ArgoCDNamespace,
	}
	_, err := controllerutil.CreateOrUpdate(
		ctx, r.Client, &sec,
		func() error {
			if sec.ObjectMeta.Labels == nil {
				sec.ObjectMeta.Labels = make(map[string]string)
			}
			sec.ObjectMeta.Labels["argocd.argoproj.io/secret-type"] = "repository"
			var ref = &corev1.Secret{}
			refMeta := client.ObjectKey{
				Name:      clusterDef.Spec.CredentialName,
				Namespace: clusterDef.Namespace,
			}
			if err := r.Client.Get(ctx, refMeta, ref); err == nil {
				if sec.Data == nil {
					sec.Data = make(map[string][]byte)
				}
				for key, val := range ref.Data {
					sec.Data[key] = val
				}
				sec.Data["url"] = []byte(clusterDef.Spec.Source.Repository)
				return nil
			} else {
				return errors.Wrap(err, "Cannot access referenced secret")
			}
		},
	)
	return errors.Wrap(err, "Credential creation failed")
}

func (r *ClusterDefReconciler) createSecret(
	clusterDef *gitopsv1.ClusterDef,
	brokerId string,
	username string,
	password string,
) (string, string, corev1.Secret) {
	var bmpSecret corev1.Secret
	secretNamePrefix := []byte(fmt.Sprintf("%s-%s", clusterDef.Name, brokerId))
	secretNamePrefixSha256 := sha256.Sum256(secretNamePrefix)
	secretNamePrefixSha256Hex := fmt.Sprintf("%x", secretNamePrefixSha256[:])

	secretName := fmt.Sprintf("%s-broker-secret", secretNamePrefixSha256Hex[0:10])
	secretNamespace := r.getTargetClusterNamespace(clusterDef)
	bmpSecret.ObjectMeta = metav1.ObjectMeta{
		Name:      secretName,
		Namespace: secretNamespace,
	}
	bmpSecret.Data = make(map[string][]byte)
	bmpSecret.Data["username"] = []byte(username)
	bmpSecret.Data["password"] = []byte(password)

	return secretName, secretNamespace, bmpSecret
}

// deploySecret create the secret for baremetalpool/network
func (r *ClusterDefReconciler) deploySecret(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	brokerId string,
	username string,
	password string,
	cache *ClusterDefStateCache,
	log logr.Logger,
) error {

	secretName, secretNamespace, bmpSecret := r.createSecret(clusterDef, brokerId, username, password)

	if !cache.IsClusterPivoted {
		_, err := controllerutil.CreateOrUpdate(ctx, r.Client, &bmpSecret, func() error { return nil })
		if err != nil {
			return fmt.Errorf("cannot create baremetalpool secret: %w", err)
		}
	} else {
		mSecret, err := runtime.DefaultUnstructuredConverter.ToUnstructured(&bmpSecret)
		if err != nil {
			return fmt.Errorf("error while converting data to unstructured: %w", err)
		}
		uSecret := &unstructured.Unstructured{Object: mSecret}

		clientConfig := clientcmd.NewDefaultClientConfig(
			*cache.Kubeconfig,
			&clientcmd.ConfigOverrides{},
		)

		restConfigTarget, _ := clientConfig.ClientConfig()

		dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
		if err != nil {
			log.Error(err, "Problem while creating client for target cluster.")
			return err
		}

		gvrsecret := schema.GroupVersionResource{
			Group:    "",
			Version:  "v1",
			Resource: "secrets",
		}

		_, err = dynClientTarget.Resource(gvrsecret).Namespace(secretNamespace).Get(ctx, secretName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				return fmt.Errorf("error while getting secret on target cluster: %w", err)
			} else {
				_, err := dynClientTarget.Resource(gvrsecret).Namespace(secretNamespace).Create(ctx, uSecret, metav1.CreateOptions{})
				if err != nil {
					return fmt.Errorf("error while creating secret on target cluster: %w", err)
				}
			}
		} else {
			_, err := dynClientTarget.Resource(gvrsecret).Namespace(secretNamespace).Update(ctx, uSecret, metav1.UpdateOptions{})
			if err != nil {
				return fmt.Errorf("error while updating secret on target cluster: %w", err)
			}
		}

	}

	return nil
}

func (r *ClusterDefReconciler) NewHttpClient(Certificate string) *http.Client {
	pool := x509.NewCertPool()
	if Certificate != "" {
		pool.AppendCertsFromPEM([]byte(Certificate))
	}
	tlsConfig := &tls.Config{
		RootCAs: pool,
	}
	transport := &http.Transport{TLSClientConfig: tlsConfig}
	return &http.Client{Transport: transport}
}

func (r *ClusterDefReconciler) copyConfigMap(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	cmName string,
) error {
	var config = &corev1.ConfigMap{}
	cmConfig := client.ObjectKey{
		Name:      cmName,
		Namespace: clusterDef.Namespace,
	}
	err := r.Client.Get(ctx, cmConfig, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the configmap", "configmapName", cmName, "namespace", clusterDef.Namespace)
		return err
	}
	config.Namespace = cache.ClusterNamespace
	config.SetResourceVersion("")
	_, err = controllerutil.CreateOrUpdate(ctx, r.Client, config, func() error { return nil })
	if err != nil {
		r.Log.Error(err, "Cannot create the configmap", "configmapName", config.Name, "namespace", config.Namespace)
		return err
	}
	return nil
}

func (r *ClusterDefReconciler) copyHostQuota(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	hostQuotaName string,
) error {
	hostQuota := &hostquota.HostQuota{}
	hostQuotaKey := client.ObjectKey{
		Name:      hostQuotaName,
		Namespace: clusterDef.Namespace,
	}
	err := r.Client.Get(ctx, hostQuotaKey, hostQuota)
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			r.Log.Error(err, "Cannot find the hostQuota", "name", hostQuota.Name, "namespace", hostQuota.Namespace)
			return err
		} else {
			r.Log.Info("hostQuota missing in local cluster, ignoring it", "name", hostQuota.Name, "namespace", hostQuota.Namespace)
			return nil
		}
	}

	hostQuota.Namespace = cache.ClusterNamespace
	hostQuota.SetResourceVersion("")
	_, err = controllerutil.CreateOrUpdate(ctx, r.Client, hostQuota, func() error { return nil })
	if err != nil {
		r.Log.Error(err, "Cannot create the hostQuota", "name", hostQuota.Name, "namespace", hostQuota.Namespace)
		return err
	}
	return nil
}

func (r *ClusterDefReconciler) getPoolUserSecret(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	poolUser *gitopsv1.PoolUserSpec,
	log logr.Logger,
) (string, error) {

	var config = &corev1.ConfigMap{}
	cmConfig := client.ObjectKey{
		Name:      poolUser.BrokerConfig,
		Namespace: clusterDef.Namespace,
	}
	err := r.Client.Get(ctx, cmConfig, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the configmap", "configmapName", poolUser.BrokerConfig, "namespace", clusterDef.Namespace)
		return "", err
	}
	brokerCA, ok := config.Data["broker_ca"]
	if !ok {
		r.Log.Error(err, "Missing broker_ca data in configmap", "configmapName", poolUser.BrokerConfig)
		return "", err
	}

	var brokerCredentials = &corev1.Secret{}
	cmCred := client.ObjectKey{
		Name:      poolUser.BrokerCredentials,
		Namespace: r.Config.InfraNamespace,
	}
	err = r.Client.Get(ctx, cmCred, brokerCredentials)
	if err != nil {
		r.Log.Error(err, "Cannot find the configmap", "configmapName", poolUser.BrokerCredentials, "namespace", r.Config.InfraNamespace)
		return "", err
	}

	if r.BrokerdefClient == nil {
		log.Info(fmt.Sprintf("Initialize brokerdef client with address %s", poolUser.Address))
		r.BrokerdefClient, err = broker.NewClientWithResponses(poolUser.Address, r.WithHttpsClient(string(brokerCA)))
		if err != nil {
			r.Log.Error(err, "can not get brokerdef http client")
			return "", err
		}
	}

	resp, err := r.BrokerdefClient.GetPooluserPassword(context.Background(),
		poolUser.Username,
		addBasicAuth(string(brokerCredentials.Data["username"]), string(brokerCredentials.Data["password"])))

	if err != nil {
		return "", err
	}

	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", err
	}

	if resp.StatusCode == 200 {
		var data PasswordData
		err = json.Unmarshal(bodyText, &data)
		if err != nil {
			log.Info(fmt.Sprintf("Error during decoding json server response : %s", fmt.Sprint(err)))
			return "", err
		}
		return data.Password, nil
	} else {
		err = fmt.Errorf("broker http response : 200 expected, received %d", resp.StatusCode)
		return "", err
	}

}

func (r *ClusterDefReconciler) getNetworkdefSecret(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	networkSpec *gitopsv1.NetworkSpec,
	log logr.Logger,
) (string, string, error) {

	var config = &corev1.ConfigMap{}
	cmConfig := client.ObjectKey{
		Name:      networkSpec.BrokerConfig,
		Namespace: clusterDef.Namespace,
	}
	err := r.Client.Get(ctx, cmConfig, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the configmap", "configmapName", networkSpec.BrokerConfig, "namespace", clusterDef.Namespace)
		return "", "", err
	}
	brokerCA, ok := config.Data["broker_ca"]
	if !ok {
		r.Log.Error(err, "Missing broker_ca data in configmap", "configmapName", networkSpec.BrokerConfig)
		return "", "", err
	}

	var brokernetCredentials = &corev1.Secret{}
	cmCred := client.ObjectKey{
		Name:      networkSpec.BrokerCredentials,
		Namespace: r.Config.InfraNamespace,
	}
	err = r.Client.Get(ctx, cmCred, brokernetCredentials)
	if err != nil {
		r.Log.Error(err, "Cannot find the configmap", "configmapName", networkSpec.BrokerCredentials, "namespace", r.Config.InfraNamespace)
		return "", "", err
	}

	if r.BrokernetClient == nil {
		r.BrokernetClient = r.NewHttpClient(brokerCA)
	}

	brokerUrl := fmt.Sprintf("%s/networkdefinition/%s/getpassword", networkSpec.Address, networkSpec.NetworkdefName)

	req, _ := http.NewRequest(http.MethodGet, brokerUrl, nil)
	req.SetBasicAuth(string(brokernetCredentials.Data["username"]), string(brokernetCredentials.Data["password"]))

	resp, err := r.BrokernetClient.Do(req)
	if err != nil {
		return "", "", err
	}
	bodyText, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", "", err
	}

	if resp.StatusCode == 200 {
		var data NetDefCredentials
		err = json.Unmarshal(bodyText, &data)
		if err != nil {
			log.Info(fmt.Sprintf("Error during decoding json server response : %s", fmt.Sprint(err)))
			return "", "", err
		}

		return data.Username, data.Password, nil
	} else {
		err = fmt.Errorf("brokernet http response : 200 expected, received %d", resp.StatusCode)
		log.Error(err, "Bad response while trying to get networkd-def secret", "url", brokerUrl, "status", resp.StatusCode)
		return "", "", err
	}

}

func (r *ClusterDefReconciler) createTargetNamespace(
	ctx context.Context,
	nsName string,
	log logr.Logger,
) error {

	nsObject := &corev1.Namespace{}

	key := client.ObjectKey{
		Name: nsName,
	}

	err := r.Get(ctx, key, nsObject)
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting namespace in local cluster", "ns", nsName)
			return err
		} else {
			nsObject := &corev1.Namespace{
				ObjectMeta: metav1.ObjectMeta{Name: nsName},
			}
			err := r.Create(ctx, nsObject)
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating namespace %s in local cluster", nsName))
				return err
			}
		}
	} else {
		return nil
	}
	log.Info(fmt.Sprintf("namespace %s created in local cluster\n", nsName))
	return nil

}
