package controllers

import (
	"context"
	"encoding/json"

	"github.com/go-logr/logr"
	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
)

func (r *ClusterDefReconciler) createNetworkAttachmentDefinition(
	ctx context.Context,
	nsName string,
	multusNetwork *gitopsv1.MultusNetwork,
	log logr.Logger) error {
	plugin := map[string]interface{}{
		"type":   "bridge",
		"bridge": multusNetwork.Bridge,
		"ipam":   map[string]interface{}{},
	}
	if multusNetwork.Vlan != nil {
		plugin["vlan"] = *&multusNetwork.Vlan
	}
	config := map[string]interface{}{
		"cniVersion": "0.3.1",
		"name":       multusNetwork.Name,
		"plugins":    []map[string]interface{}{plugin},
	}
	configBytes, err := json.Marshal(config)
	if err != nil {
		log.Error(err, "Cannot marshal Multus NAD config to string", "multusName", multusNetwork.Name)
		return err
	}
	obj := unstructured.Unstructured{}
	obj.SetGroupVersionKind(schema.GroupVersionKind{
		Group:   "k8s.cni.cncf.io",
		Version: "v1",
		Kind:    "NetworkAttachmentDefinition",
	})
	obj.SetName(multusNetwork.Name)
	obj.SetNamespace(nsName)
	_, err = controllerutil.CreateOrUpdate(
		ctx, r.Client, &obj,
		func() error {
			obj.Object["spec"] = map[string]interface{}{
				"config": string(configBytes),
			}
			return nil
		})

	if err != nil {
		log.Error(err, "Cannot create NetworkAttachmentDefinition for network", "multusNetwork", multusNetwork.Name)
		return err
	}

	return nil
}
