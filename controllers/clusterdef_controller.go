/*
Copyright 2020-22 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"
	"net/http"
	"strings"
	"time"

	kcpkamajiv1alpha1 "github.com/clastix/cluster-api-control-plane-provider-kamaji/api/v1alpha1"

	"github.com/go-logr/logr"
	broker "gitlab.com/Orange-OpenSource/kanod/brokerdef/broker"
	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	argocd "gitlab.com/Orange-OpenSource/kanod/cluster-def/thirdparty/argocd/apis/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apiextensions-apiserver/pkg/apis/apiextensions"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	clientgo "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	clientapi "k8s.io/client-go/tools/clientcmd/api"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"
	"sigs.k8s.io/controller-runtime/pkg/handler"

	capi "sigs.k8s.io/cluster-api/api/v1beta1"

	cpcapi "sigs.k8s.io/cluster-api/controlplane/kubeadm/api/v1beta1"
	logger "sigs.k8s.io/controller-runtime/pkg/log"
)

const CAPI_VERSION = "v1beta1"

var (
	appOwnerKey = ".metadata.clusterdef"
	apiGVStr    = gitopsv1.GroupVersion.String()
)

// ClusterDefReconciler reconciles a ClusterDef object
type ClusterDefReconciler struct {
	client.Client
	Log             logr.Logger
	Scheme          *runtime.Scheme
	Config          *ClusterDefConfig
	State           map[string]*ClusterDefStateCache
	BrokerdefClient *broker.ClientWithResponses
	BrokernetClient *http.Client
}

// ClusterDefStateCache maintains the transient state of the reconciler.

type ClusterDefStateCache struct {
	// ClusterName is the name of target cluster-api definition
	ClusterName string
	// ClusterNamespace is the namespace of target cluster-api definition
	ClusterNamespace string
	// Kubeconfig is the config of target
	Kubeconfig *clientapi.Config
	// ServerURL is the cached URL of the workload cluster API endpoint
	ServerURL string
	// TargetClient is a kubernetes client for the target (workload) cluster
	TargetClient *clientgo.Clientset
	// isClusterPivoted
	IsClusterPivoted bool
	// isClusterReady
	IsClusterReady bool
	// PivotStatus
	PivotStatus gitopsv1.PivotState
}

// TransientState is the state of reconciler during the pivoting process
type TransientState int

const (
	// TSNotPivoted
	TSNotPivoted TransientState = iota
	// TSStoreTargetClusterInfo
	TSStoreTargetClusterInfo
	// TSUndeployLocalIronic
	TSUndeployLocalIronic
	// TSCheckIronicIsUndeployed
	TSCheckIronicIsUndeployed
	// TSInstallKanodOperator
	TSInstallKanodOperator
	// TSWaitForKanodOperator
	TSWaitForKanodOperator
	// TSMoveKanod
	TSMoveKanod
	// TSWaitForStackDeployed
	TSWaitForStackDeployed
	// TSDeleteArgoCdApp
	TSDeleteArgoCdApp
	// TSLabelResources
	TSLabelResources
	// TSPauseBareMetalPool
	TSPauseBareMetalPool
	// TSPauseByoHostPool
	TSPauseByoHostPool
	// TSPauseAndConfigureByoHost
	TSPauseAndConfigureByoHost
	// TSPauseNetwork
	TSPauseNetwork
	// TSPivotNetworkResources
	TSPivotNetworkResources
	// TSPivotResources
	TSPivotResources
	// TSUnlabelResources
	TSUnlabelResources
	// TSPivotBmpResources
	TSPivotBmpResources
	// TSPivotByoHostPoolResources
	TSPivotByoHostPoolResources
	// TSActivateNetwork
	TSActivateNetwork
	// TSActivateBareMetalPool
	TSActivateBareMetalPool
	// TSActivateByoHost
	TSActivateByoHost
	// TSActivateBareMetalPool
	TSActivateByoHostPool
	// TSRedeployLocalIronic
	TSRedeployLocalIronic
	// TSCheckIronicIsdeployed
	TSCheckIronicIsdeployed
	// TSConfigureArgoApp
	TSConfigureArgoApp
	// ClusterPivoted
	TSClusterPivoted
)

// ClusterDefConfig contains global parameters of the ClusterDef operator.
// Typical operators are the location and credentials of the snapshot repository,
// location of base cluster models and infrastructure parameters.
type ClusterDefConfig struct {
	ArgoCDNamespace   string
	ClusterFileName   string
	PluginName        string
	InfraNamespace    string
	KanodOperatorCrd  string
	MilliResyncPeriod int
}

type Env struct {
	Env    map[string]string
	Errors []string
}

func (e *Env) Getenv(key string) string {
	v, ok := e.Env[key]
	if !ok {
		e.Errors = append(e.Errors, key)
	}
	return v
}

func (e *Env) Setenv(key string, val string) {
	e.Env[key] = val
}

// MilliResyncPeriod is the duration between two synchronization attempt of a given resource
const MilliResyncPeriod = 10000

const PivotMilliResyncPeriod = 5000

// finalizerName is the name of the finalizer used for cascade resource deletion
const finalizerName = "resources-finalizer.gitops.kanod.io"

// simpleFinalizerName is the name of the finalizer used for simple resource deletion (no cascade)
const simpleFinalizerName = "simple-resources-finalizer.gitops.kanod.io"

// bmpAnnotationPaused is the name of the annotation for pausing baremetalpool controller
const bmpAnnotationPaused = "baremetalpool.kanod.io/paused"

// bhpscalerAnnotationPaused is the name of the annotation for pausing byohostpool-scaler controller
const bhpscalerAnnotationPaused = "byohostpoolscaler.kanod.io/paused"

// byoHostPoolPausedAnnotation is the name of the annotation for pausing byohostpool controller
const byoHostPoolPausedAnnotation = "byohostpool.kanod.io/paused"

// bootstrapKubeconfigdAnnotation is the name of the annotation containing the byoHost bootstrapKubeconfig
const bootstrapKubeconfigdAnnotation = "byoh.infrastructure.cluster.x-k8s.io/bootstrapkubeconfig"

// networkAnnotationPaused is the name of the annotation for pausing network operator
const networkAnnotationPaused = "netpool.network.kanod.io/paused"

// dhcpconfigAnnotationPaused is the name of the annotation for pausing dhcpconfig operator
const dhcpconfigAnnotationPaused = "dhcpconfig.kanod.io/paused"

// clusterdefAnnotationPaused is the name of the annotation for pausing dhcpconfig operator
const clusterdefAnnotationPaused = "clusterdef.kanod.io/paused"

// BmhPausedAnnotation
const bmhPausedAnnotation = "baremetalhost.metal3.io/paused"

// BmhPausedAnnotationKey
const bmhPausedAnnotationKey = "metal3.io/capm3"

// bmhPausedAnnotationKanodKey
const bmhPausedAnnotationKanodKey = "kanod.io/clusterdef"

// byoHostStatusAnnotation is the name of the annotation for storing byoHost status
const byoHostStatusAnnotation = "byohost.kanod.io/status"

// baremetalpoolFinalizer is the name of the finalizer for baremetalpool resource
const baremetalpoolFinalizer = "baremetalpool.kanod.io/finalizer"

// networkFinalizer is the name of the finalizer for network resource
const networkFinalizer = "netpool.network.kanod.io/finalizer"

// LocalK8sApiServer is the address of the API server got the local cluster
const LocalK8sApiServer = "https://kubernetes.default.svc"

// clusterUrlAnnotation is the name of the annotation containing the pivoted cluster url
const clusterUrlAnnotation = "clusterdef.kanod.io/cluster-url"

// clusterPivotedAnnotation is the name of the annotation indicating if the cluster is pivoted
const clusterPivotedAnnotation = "clusterdef.kanod.io/cluster-pivoted"

// apiServerByoHostPoolScalerAnnotation
const apiServerByoHostPoolScalerAnnotation = "kanod.io/apiserver"

// Name of the Ironic deployment
const ironicDeploymentName = "ironic"

// Namespace of the Ironic deployment
const ironicDeploymentNamespace = "baremetal-operator-system"

// Name of the Kanod operator deployment
const kanodDeploymentName = "kanod-operator-controller-manager"

// Namespace of the Kanod operator deployment
const kanodDeploymentNamespace = "kanod-operator-system"

// name of the KamajiControlPlane CRD
const kamajiControlPlaneCrd = "kamajicontrolplanes.controlplane.cluster.x-k8s.io"

// annotation for the move process
const PROGRESSION_ANNOTATION = "clusterdef.kanod.io/progression"

// +kubebuilder:rbac:groups=gitops.kanod.io,resources=clusterdefs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=gitops.kanod.io,resources=clusterdefs/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=argoproj.io,resources=applications,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="",resources=secrets,verbs=get;list;watch;create;update;delete;patch
// +kubebuilder:rbac:groups="",resources=configmaps,verbs=get;list;watch;create;update;delete;patch
// +kubebuilder:rbac:groups="",resources=namespaces,verbs=get;list;watch;create;update;delete;patch
// +kubebuilder:rbac:groups="apiextensions.k8s.io",resources=customresourcedefinitions,verbs=get;list;watch;create;update;patch
// +kubebuilder:rbac:groups="cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="infrastructure.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="addons.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="bootstrap.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="controlplane.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="metal3.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="ipam.metal3.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="kanod.io",resources="hosts",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="ipam.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="runtime.cluster.x-k8s.io",resources="*",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="clusterctl.cluster.x-k8s.io",resources="providers",verbs=get;list
// +kubebuilder:rbac:groups="bmp.kanod.io",resources="baremetalpools",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="netpool.kanod.io",resources="networks",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="config.kanod.io",resources="kanods",verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups="apps",resources="deployment",verbs=get;list;update;patch
// +kubebuilder:rbac:groups="apps",resources="deployments",verbs=get;list;update;patch;watch
// +kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpoolscalers,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=pools.kanod.io,resources=byohostpools,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=metal3.io,resources=baremetalhosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byomachines,verbs=get;list;watch
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byohosts,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=infrastructure.cluster.x-k8s.io,resources=byohosts/status,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=network.kanod.io,resources=dhcpconfigs,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=kanod.io,resources=hostquotas,verbs=get;list;watch;create;update;patch;delete
//+kubebuilder:rbac:groups=k8s.cni.cncf.io,resources=network-attachment-definitions,verbs=get;list;watch;create;update;patch;delete

// Reconcile implements the core logic
func (r *ClusterDefReconciler) Reconcile(ctx context.Context, req ctrl.Request) (ctrl.Result, error) {
	log := r.Log.WithValues("clusterdef", req.NamespacedName)
	var clusterDef gitopsv1.ClusterDef

	fullname := fmt.Sprintf("%s/%s", req.Namespace, req.Name)

	if err := r.Get(ctx, req.NamespacedName, &clusterDef); err != nil {
		delete(r.State, fullname)
		return ctrl.Result{}, client.IgnoreNotFound(err)
	}

	if !clusterDef.ObjectMeta.DeletionTimestamp.IsZero() {
		return r.finalizeDeletion(req.Name, &clusterDef, log)
	} else {
		if !controllerutil.ContainsFinalizer(&clusterDef, simpleFinalizerName) {
			controllerutil.AddFinalizer(&clusterDef, simpleFinalizerName)
			if err := r.Update(ctx, &clusterDef); err != nil {
				return ctrl.Result{}, err
			}
		}
	}

	if clusterDef.Annotations != nil {
		if _, ok := clusterDef.Annotations[clusterdefAnnotationPaused]; ok {
			log.Info("clusterDef controller is paused")
			return ctrl.Result{}, nil
		}
	}

	cache, presence := r.State[fullname]

	if !presence {
		cache = &ClusterDefStateCache{
			ClusterName:      "",
			ClusterNamespace: "",
			Kubeconfig:       nil,
			ServerURL:        LocalK8sApiServer,
			PivotStatus:      gitopsv1.NotPivoted,
			IsClusterPivoted: false,
			IsClusterReady:   false,
		}
		r.State[fullname] = cache
		log.Info("Initial registration in cache completed")
	}

	if !presence {
		if err := r.initializeState(ctx, &clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while initializing state.")
			return ctrl.Result{}, err
		}
	}

	if err := r.makeCredentials(ctx, &clusterDef); err != nil {
		log.Error(err, "Problem with application credentials creation.")
		return ctrl.Result{}, err
	}

	if err := r.createTargetNamespace(ctx, cache.ClusterNamespace, log); err != nil {
		log.Error(err, "error when creating namespace in local cluster", "namespace", cache.ClusterNamespace)
		return ctrl.Result{}, err
	}

	log.V(1).Info("Checking multus networks")
	for _, multusNetwork := range clusterDef.Spec.MultusNetworks {
		log.V(1).Info("handling multus network", "multusNetwork", multusNetwork.Name)
		r.createNetworkAttachmentDefinition(ctx, cache.ClusterNamespace, &multusNetwork, log)
	}

	for brokerId, infoBroker := range clusterDef.Spec.PoolUserList {
		secret, err := r.getPoolUserSecret(ctx, &clusterDef, &infoBroker, log)
		if err != nil {
			log.Info("waiting for brokerdef server available")
			return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
		}
		if err := r.deploySecret(ctx, &clusterDef, brokerId, infoBroker.Username, secret, cache, log); err != nil {
			log.Error(err, "Problem with baremetalpool secret creation.")
			return ctrl.Result{}, err
		}

		if err := r.copyConfigMap(ctx, &clusterDef, cache, infoBroker.BrokerConfig); err != nil {
			log.Error(err, "cannot copy configmap in cluster namespace.")
			return ctrl.Result{}, err
		}
	}

	for networkId, infoBrokernet := range clusterDef.Spec.NetworkList {
		username, secret, err := r.getNetworkdefSecret(ctx, &clusterDef, &infoBrokernet, log)
		if err != nil {
			log.Info("waiting for brokernet server available")
			return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
		}
		if err := r.deploySecret(ctx, &clusterDef, networkId, username, secret, cache, log); err != nil {
			log.Error(err, "Problem with network definition secret creation.")
			return ctrl.Result{}, err
		}
		if err := r.copyConfigMap(ctx, &clusterDef, cache, infoBrokernet.BrokerConfig); err != nil {
			log.Error(err, "cannot copy configmap in cluster namespace.")
			return ctrl.Result{}, err
		}
	}

	for _, hostQuotaName := range clusterDef.Spec.HostQuotaList {
		if err := r.copyHostQuota(ctx, &clusterDef, cache, hostQuotaName); err != nil {
			log.Error(err, "cannot copy HostQuota in cluster namespace.", "hostQuotaName", hostQuotaName)
			return ctrl.Result{}, err
		}
	}

	if clusterDef.Spec.PivotInfo.Pivot && !cache.IsClusterPivoted {
		err, isReady := r.isClusterReadyForPivot(ctx, &clusterDef, log)
		if err != nil {
			log.Error(err, "Problem while checking if cluster is ready for pivot.")
			return ctrl.Result{}, err
		}

		if isReady || cache.PivotStatus != gitopsv1.NotPivoted {
			return r.launchPivotProcess(ctx, log, cache, &clusterDef, req)
		}

	}

	serverUrl := cache.ServerURL
	if err := r.makeApplication(ctx, &clusterDef, serverUrl); err != nil {
		log.Error(err, "Problem with application creation.")
		return ctrl.Result{}, err
	}

	phase, cpVersion, versions := r.computeStatus(ctx, &clusterDef, log, cache)

	if phase == gitopsv1.Ready {
		cache.IsClusterReady = true
	}

	if phase == gitopsv1.ClusterProvisionned && !cache.IsClusterPivoted {
		if clusterDef.Spec.CreateArgocdClusterSecret {
			log.Info("creating argocd secret for target cluster")
			kubeConfig, err := r.getKubeConfig(ctx, clusterDef.Name, clusterDef.Namespace)
			if err != nil {
				r.Log.Error(err, "unable to get kubeconfig for cluster", "cluster name", clusterDef.Name)
				return ctrl.Result{}, err
			}
			if kubeConfig == nil {
				return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
			}
			err = r.UpdateArgoCDConfig(ctx, clusterDef.Name, kubeConfig)
			if err != nil {
				r.Log.Error(err, "unable to create argocd secret for cluster", "cluster name", clusterDef.Name)
				return ctrl.Result{}, err
			}
		}
	}

	if err := r.UpdateStatus(req.NamespacedName, phase, cpVersion, versions, cache.PivotStatus); err != nil {
		log.Error(err, "Problem while updating status.")
		return ctrl.Result{}, err
	}
	if needResync(&clusterDef, cpVersion, versions, log) {
		if err := r.syncApp(ctx, &req.NamespacedName, log); err != nil {
			log.Error(err, "Problem while forcing resync")
			return ctrl.Result{}, err
		}
	}

	return ctrl.Result{RequeueAfter: MilliResyncPeriod * time.Millisecond}, nil
}

func hasCrd(mgr ctrl.Manager, name string) bool {
	crd := &apiextensions.CustomResourceDefinition{}
	key := types.NamespacedName{Name: name}
	ctx := context.Background()
	err := mgr.GetAPIReader().Get(ctx, key, crd)
	if err != nil {
		logger.Log.Info("Cannot find CRD", "crd", name)
	}
	return err == nil
}

// SetupWithManager registers the controller for ClusterDef
func (r *ClusterDefReconciler) SetupWithManager(mgr ctrl.Manager) error {

	if err := mgr.GetFieldIndexer().IndexField(context.Background(), &argocd.Application{}, appOwnerKey, indexFunc); err != nil {
		return err
	}

	mgr1 := ctrl.NewControllerManagedBy(mgr).
		For(&gitopsv1.ClusterDef{}).
		Owns(&argocd.Application{}).
		Watches(
			&capi.Machine{},
			handler.EnqueueRequestsFromMapFunc(r.MapObjectToClusterDef),
		).
		Watches(
			&cpcapi.KubeadmControlPlane{},
			handler.EnqueueRequestsFromMapFunc(r.MapObjectToClusterDef),
		)

	if hasCrd(mgr, kamajiControlPlaneCrd) {
		logger.Log.Info("Support for kamaji: ok")
		mgr1.Watches(
			&kcpkamajiv1alpha1.KamajiControlPlane{},
			handler.EnqueueRequestsFromMapFunc(r.MapObjectToClusterDef))
	} else {
		logger.Log.Info("No support for kamaji")
	}

	return mgr1.Complete(r)

}

// MakeConfig initializes the global configuration of the controller
func MakeConfig() *ClusterDefConfig {
	return &ClusterDefConfig{
		ArgoCDNamespace:  getEnvDefault("ARGOCD_NAMESPACE", "argocd"),
		PluginName:       getEnvDefault("ARGOCD_PLUGIN_NAME", "argocd-updater-plugin"),
		ClusterFileName:  getEnvDefault("CLUSTER_FILE_NAME", "config.yaml"),
		InfraNamespace:   getEnvDefault("INFRA_NAMESPACE", "cluster-def"),
		KanodOperatorCrd: getEnvDefault("KANOD_OPERATOR_CRD", ""),
	}
}

// indexFunc is an indexer for applications based on ownership by clusterdef
func indexFunc(rawObj client.Object) []string {
	app := rawObj.(*argocd.Application)
	owner := metav1.GetControllerOf(app)
	if owner == nil {
		return nil
	}
	if owner.APIVersion != apiGVStr || owner.Kind != "ClusterDef" {
		return nil
	}

	// ...and if so, return it
	return []string{owner.Name}
}

// Get cluster status after controller start
// and initialize cache
func (r *ClusterDefReconciler) initializeState(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger,
) error {
	var config *clientapi.Config
	var annotStep string

	log.Info("Check cluster state after controller start")

	clusterUrl, present := clusterDef.Annotations[clusterUrlAnnotation]
	if !present {
		clusterUrl = LocalK8sApiServer
	}

	clusterPivoted, present := clusterDef.Annotations[clusterPivotedAnnotation]
	if !present {
		clusterPivoted = "false"
	}

	// check server address of the cluster
	if clusterPivoted == "true" {
		var cluster capi.Cluster

		argoSecretForTargetCluster := corev1.Secret{}
		secretKey := client.ObjectKey{
			Name:      fmt.Sprintf("%s-config", clusterDef.Name),
			Namespace: r.Config.ArgoCDNamespace,
		}
		if err := r.Get(ctx, secretKey, &argoSecretForTargetCluster); err != nil {
			if k8serrors.IsNotFound(err) {
				log.Error(err, "Argo secret for target cluster missing")
			}
			return err
		} else {
			config, err = createKubeconfigFromArgoSecret(clusterDef.Name, argoSecretForTargetCluster)
			if err != nil {
				log.Error(err, "Cannot create kubeconfig from argo secret")
				return err
			}
		}

		restClient := getClientWithKubeconfig(config, &capi.GroupVersion)
		targetClusterNamespace := r.getTargetClusterNamespace(clusterDef)
		err := restClient.Get().Namespace(targetClusterNamespace).Resource("clusters").Name(clusterDef.Name).Do(context.TODO()).Into(&cluster)
		if err != nil {
			log.Error(err, "Cannot get Cluster info")
			return err
		}
		cache.Kubeconfig = config
		cache.ServerURL = clusterUrl
		cache.IsClusterPivoted = true
		cache.PivotStatus = gitopsv1.ClusterPivoted
		cache.ClusterName = cluster.GetName()
		cache.ClusterNamespace = cluster.GetNamespace()

	} else {
		var config *clientapi.Config
		progession := TSNotPivoted
		annot := clusterDef.GetAnnotations()
		annotStep, ok := annot[PROGRESSION_ANNOTATION]
		if ok {
			var recDate int64
			fmt.Sscanf(annotStep, "%d/%d", &progession, &recDate)
		}

		if progession != TSNotPivoted {
			argoSecretForTargetCluster := corev1.Secret{}
			secretKey := client.ObjectKey{
				Name:      fmt.Sprintf("%s-config", clusterDef.Name),
				Namespace: r.Config.ArgoCDNamespace,
			}
			if err := r.Get(ctx, secretKey, &argoSecretForTargetCluster); err != nil {
				if k8serrors.IsNotFound(err) {
					log.Error(err, "Argo secret for target cluster missing")
				}
				return err
			} else {
				config, err = createKubeconfigFromArgoSecret(clusterDef.Name, argoSecretForTargetCluster)
				if err != nil {
					log.Error(err, "Cannot create kubeconfig from argo secret")
					return err
				}
			}
		}

		cache.Kubeconfig = config
		cache.ServerURL = clusterUrl
		cache.IsClusterPivoted = false
		cache.PivotStatus = gitopsv1.NotPivoted
		cache.ClusterName = clusterDef.Name
		cache.ClusterNamespace = r.getTargetClusterNamespace(clusterDef)
	}

	log.Info("clusterdef controller initialized", "clusterUrl :", clusterUrl)
	log.Info("clusterdef controller initialized", "clusterPivoted :", clusterPivoted)
	log.Info("clusterdef controller initialized", "ClusterNamespace :", cache.ClusterNamespace)
	log.Info("clusterdef controller initialized", "annotion step :", annotStep)

	return nil
}

func (r *ClusterDefReconciler) launchPivotProcess(
	ctx context.Context,
	log logr.Logger,
	cache *ClusterDefStateCache,
	clusterDef *gitopsv1.ClusterDef,
	req ctrl.Request,
) (ctrl.Result, error) {

	step, recDate, err := r.getProgressionWithClient(ctx, req.NamespacedName)
	if err != nil {
		r.setProgression(ctx, clusterDef, 0)
		return ctrl.Result{}, err
	}

	log.Info("Progression : ", "step", step, "recDate", recDate)
	switch step {
	case TSNotPivoted:
		log.Info("Pause clusterapi")
		err = r.pauseCapi(ctx, clusterDef)
		if err != nil {
			log.Error(err, "Problem while pausing clusterapi.")
			return ctrl.Result{}, err
		}
		if err := r.setProgression(ctx, clusterDef, TSStoreTargetClusterInfo); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSStoreTargetClusterInfo")
			return ctrl.Result{}, err
		}

	case TSStoreTargetClusterInfo:
		log.Info("StoreTargetClusterInfo...")
		if err := r.StoreTargetClusterInfo(ctx, cache, clusterDef, log); err != nil {
			log.Error(err, "Problem while get target cluster info")
			return ctrl.Result{}, err
		}

		log.Info(fmt.Sprintf("creating %s namespace in target cluster", cache.ClusterNamespace))
		if err := r.createNamespace(ctx, cache, cache.ClusterNamespace, log); err != nil {
			log.Error(err, "Error while creating cluster namespace in target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.TargetClusterInfoStored
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSUndeployLocalIronic); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSUndeployLocalIronic")
			return ctrl.Result{}, err
		}

	case TSUndeployLocalIronic:
		log.Info("scale ironic deployment to 0 on local cluster")
		err := r.scaleLocalIronicDeployment(ctx, 0)
		if err != nil {
			log.Error(err, "Problem while scaling to 0 ironic deployment cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.IronicOnLocalClusterScaledToZero

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSCheckIronicIsUndeployed); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSCheckIronicIsUndeployed")
			return ctrl.Result{}, err
		}

	case TSCheckIronicIsUndeployed:
		log.Info("Wait for ironic deployment being deployed")
		err, isIronicUndeployed := r.isIronicDeploymentReady(ctx, log)
		if err != nil {
			log.Error(err, "Problem while checking if ironic is undeployed")
			return ctrl.Result{}, err
		}

		if !isIronicUndeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.IronicOnLocalClusterUndeployed

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSInstallKanodOperator); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSInstallKanodOperator")
			return ctrl.Result{}, err
		}

	case TSInstallKanodOperator:
		log.Info("Installing kanod operator...")
		if err := r.installKanodOperator(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving kanod custom resource on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.KanodOperatorInstalled
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSWaitForKanodOperator); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSWaitForKanodOperator")
			return ctrl.Result{}, err
		}

	case TSWaitForKanodOperator:
		log.Info("waiting for kanod operator...")
		err, isDeployed := r.isKanodDeployed(ctx, cache, log)
		if err != nil {
			log.Error(err, "Problem while checking if kanod operator is deployed on target cluster")
			return ctrl.Result{}, err
		}

		if !isDeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.KanodOperatorReady
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSMoveKanod); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSMoveKanod")
			return ctrl.Result{}, err
		}

	case TSMoveKanod:
		log.Info("Moving kanod resource...")
		if err := r.createNamespace(ctx, cache, clusterDef.Spec.PivotInfo.KanodNamespace, log); err != nil {
			log.Error(err, "Error while creating namespace for kanod on target cluster")
			return ctrl.Result{}, err
		}

		if err := r.moveKanodResources(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving kanod custom resource on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.KanodResourcesMoved
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSWaitForStackDeployed); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSWaitForStackDeployed")
			return ctrl.Result{}, err
		}

	case TSWaitForStackDeployed:
		log.Info("Wait for stack deployment...")
		err, isStackDeployed := r.isStackDeployed(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while checking if stack is deployed on target cluster")
			return ctrl.Result{}, err
		}

		if !isStackDeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.StackDeployed
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue

		if err := r.setProgression(ctx, clusterDef, TSDeleteArgoCdApp); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSDeleteArgoCdApp")
			return ctrl.Result{}, err
		}

	case TSDeleteArgoCdApp:
		log.Info("Delete ArgoCD application")
		appName := fmt.Sprintf("cdef-%s", clusterDef.Name)
		r.deleteArgocdApp(ctx, appName, log)

		statusValue := gitopsv1.ArgoAppDeleted
		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSLabelResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSLabelResources")
			return ctrl.Result{}, err
		}

	case TSLabelResources:
		log.Info("Add labels on resources for clusterctl move")
		if err := r.labelResources(ctx, clusterDef, log, true); err != nil {
			log.Error(err, "Problem while labeling resources.")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ResourcesLabeled

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPauseBareMetalPool); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPauseBareMetalPool")
			return ctrl.Result{}, err
		}

	case TSPauseBareMetalPool:
		log.Info("Pause baremetalpool on local cluster")
		if err := r.pauseBareMetalPool(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while pausing baremetalpool")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.BareMetalPoolPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPauseByoHostPool); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPauseByoHostPool")
			return ctrl.Result{}, err
		}

	case TSPauseByoHostPool:
		if clusterDef.Spec.PivotInfo.ByoHostPoolUsed {
			log.Info("Pause byohostpool and byohostpool-scaler on local cluster")
			if err := r.pauseByoHostPool(ctx, clusterDef, cache, log); err != nil {
				log.Error(err, "Problem while pausing byohostpool")
				return ctrl.Result{}, err
			}

			statusValue := gitopsv1.ByoHostPoolPaused

			if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
				log.Error(err, "Problem while updating status.")
				return ctrl.Result{}, err
			}
			cache.PivotStatus = statusValue
		}
		if err := r.setProgression(ctx, clusterDef, TSPauseAndConfigureByoHost); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPauseAndConfigureByoHost")
			return ctrl.Result{}, err
		}

	case TSPauseAndConfigureByoHost:
		if clusterDef.Spec.PivotInfo.ByoHostPoolUsed {
			log.Info("Pause and configure byoHost resources on local cluster")
			ok, err := r.pauseAndConfigureByoHost(ctx, clusterDef, cache, log)
			if err != nil {
				log.Error(err, "Problem while configuring byoHost")
				return ctrl.Result{}, err
			}

			if ok {
				log.Info("waiting for bootstrapkubeconfig creation, requeuing")
				return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, err
			}
			statusValue := gitopsv1.ByoHostPausedAnConfigured

			if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
				log.Error(err, "Problem while updating status.")
				return ctrl.Result{}, err
			}
			cache.PivotStatus = statusValue
		}
		if err := r.setProgression(ctx, clusterDef, TSPauseNetwork); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPauseNetwork")
			return ctrl.Result{}, err
		}

	case TSPauseNetwork:
		log.Info("Update clustername in Ippool resources on local cluster")
		requeue, err := r.updateClusternameInIppool(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while updating clustername in Ippool resources")
			return ctrl.Result{}, err
		}

		if requeue {
			log.Info("clustername updated in Ippool resource on local cluster, requeuing")
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, err
		}

		log.Info("Pause network operator on local cluster")
		if err := r.pauseNetwork(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while pausing network operator")
			return ctrl.Result{}, err
		}

		log.Info("Pause dhcpconfig operator on local cluster")
		if err := r.pauseDhcpconfig(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while pausing dhcpconfig operator")
			return ctrl.Result{}, err
		}

		log.Info("Pause baremetalhost on local cluster")
		if err := r.pauseBareMetalHost(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while pausing baremetalhost operator")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.NetworkPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPivotNetworkResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPivotNetworkResources")
			return ctrl.Result{}, err
		}

	case TSPivotNetworkResources:
		log.Info("Pivot dhcpConfig resources on target cluster")
		if err := r.moveDhcpConfigResources(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving dhcpConfig resources on target cluster")
			return ctrl.Result{}, err
		}

		log.Info("Pivot network resources on target cluster")
		if err := r.moveNetworkResources(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving network on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.NetworkResourcesPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPivotResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPivotResources")
			return ctrl.Result{}, err
		}

	case TSPivotResources:
		log.Info("Pivot resources on target cluster")
		if err := r.moveClusterResources(ctx, cache, log); err != nil {
			log.Error(err, "Problem while moving resources on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ResourcesPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSUnlabelResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSUnlabelResources")
			return ctrl.Result{}, err
		}

	case TSUnlabelResources:
		log.Info("Remove labels on resources")
		if err := r.labelResources(ctx, clusterDef, log, false); err != nil {
			log.Error(err, "Problem while labeling resources.")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ResourcesUnlabeled

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPivotBmpResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPivotBmpResources")
			return ctrl.Result{}, err
		}

	case TSPivotBmpResources:
		log.Info("Pivot baremetalpool resources on target cluster")
		if err := r.moveBareMetalPoolResources(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while moving baremetalpool on target cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.BmpResourcesPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSPivotByoHostPoolResources); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSPivotByoHostPoolResources")
			return ctrl.Result{}, err
		}

	case TSPivotByoHostPoolResources:
		if clusterDef.Spec.PivotInfo.ByoHostPoolUsed {
			log.Info("Pivot byohostpool resources on target cluster")
			if err := r.moveByoHostPoolResources(ctx, clusterDef, cache, log); err != nil {
				log.Error(err, "Problem while moving byohostpool on target cluster")
				return ctrl.Result{}, err
			}

			statusValue := gitopsv1.ByoHostPoolResourcesPivoted

			if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
				log.Error(err, "Problem while updating status.")
				return ctrl.Result{}, err
			}

			cache.PivotStatus = statusValue
		}

		if err := r.setProgression(ctx, clusterDef, TSActivateNetwork); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSActivateNetwork")
			return ctrl.Result{}, err
		}

	case TSActivateNetwork:
		log.Info("unpause network operator resources on target cluster")
		err, ok := r.unpauseNetworkResources(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while unpausing network operator on target cluster")
			return ctrl.Result{}, err
		}

		if !ok {
			log.Info("waiting for deletion of network operator on local cluster")
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, err
		}

		log.Info("Unpause dhcpconfig operator on target cluster")
		if err := r.unPauseDhcpconfig(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while unpausing dhcpconfig operator")
			return ctrl.Result{}, err
		}

		log.Info("Unpause baremetalhost on local cluster")
		if err := r.unpauseBareMetalHost(ctx, clusterDef, cache, log); err != nil {
			log.Error(err, "Problem while unpausing baremetalhost")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.NetworkUnPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSActivateBareMetalPool); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSActivateBareMetalPool")
			return ctrl.Result{}, err
		}

	case TSActivateBareMetalPool:
		log.Info("unpause baremetalpool resources on target cluster")
		err, ok := r.unpauseBareMetalPoolResources(ctx, clusterDef, cache, log)
		if err != nil {
			log.Error(err, "Problem while unpausing baremetalpool on target cluster")
			return ctrl.Result{}, err
		}

		if !ok {
			log.Info("waiting for deletion of baremetalpool on local cluster")
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, err
		}
		statusValue := gitopsv1.BareMetalPoolUnPaused

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSActivateByoHost); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSActivateByoHost")
			return ctrl.Result{}, err
		}

	case TSActivateByoHost:
		if clusterDef.Spec.PivotInfo.ByoHostPoolUsed {
			log.Info("unpause byohost resources on target cluster")
			err := r.unpauseByoHostResources(ctx, clusterDef, cache, log)
			if err != nil {
				log.Error(err, "Problem while unpausing byohost on target cluster")
				return ctrl.Result{}, err
			}

			statusValue := gitopsv1.ByoHostUnPaused

			if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
				log.Error(err, "Problem while updating status.")
				return ctrl.Result{}, err
			}

			cache.PivotStatus = statusValue
		}

		if err := r.setProgression(ctx, clusterDef, TSActivateByoHostPool); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSActivateByoHostPool")
			return ctrl.Result{}, err
		}

	case TSActivateByoHostPool:
		if clusterDef.Spec.PivotInfo.ByoHostPoolUsed {
			log.Info("unpause byohostpool resources on target cluster")
			err, ok := r.unpauseByoHostPoolResources(ctx, clusterDef, cache, log)
			if err != nil {
				log.Error(err, "Problem while unpausing byohostpool on target cluster")
				return ctrl.Result{}, err
			}

			if !ok {
				log.Info("waiting for deletion of byohostpool on local cluster")
				return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, err
			}
			statusValue := gitopsv1.ByoHostPoolUnPaused

			if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
				log.Error(err, "Problem while updating status.")
				return ctrl.Result{}, err
			}

			cache.PivotStatus = statusValue
		}

		if err := r.setProgression(ctx, clusterDef, TSRedeployLocalIronic); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSRedeployLocalIronic")
			return ctrl.Result{}, err
		}

	case TSRedeployLocalIronic:
		log.Info("scale ironic deployment to 1 on local cluster")
		err := r.scaleLocalIronicDeployment(ctx, 1)
		if err != nil {
			log.Error(err, "Problem while scaling to 1 ironic deployment cluster")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.IronicOnLocalClusterScaledToOne

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSCheckIronicIsdeployed); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSCheckIronicIsdeployed")
			return ctrl.Result{}, err
		}

	case TSCheckIronicIsdeployed:
		log.Info("Wait for ironic deployment being deployed")
		err, isIronicRedeployed := r.isIronicDeploymentReady(ctx, log)
		if err != nil {
			log.Error(err, "Problem while checking if ironic is redeployed")
			return ctrl.Result{}, err
		}

		if !isIronicRedeployed {
			return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
		}

		statusValue := gitopsv1.IronicOnLocalClusterRedeployed

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSConfigureArgoApp); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSConfigureArgoApp")
			return ctrl.Result{}, err
		}

	case TSConfigureArgoApp:
		log.Info("Configure ArgoCD application")
		if err := r.UpdateArgoCDConfig(ctx, cache.ClusterName, cache.Kubeconfig); err != nil {
			log.Error(err, "Problem while changing destination cluster for argocd application")
			return ctrl.Result{}, err
		}

		statusValue := gitopsv1.ClusterPivoted

		if err := r.UpdatePivotStatus(req.NamespacedName, statusValue); err != nil {
			log.Error(err, "Problem while updating status.")
			return ctrl.Result{}, err
		}

		cache.IsClusterPivoted = true
		cache.PivotStatus = statusValue
		if err := r.setProgression(ctx, clusterDef, TSClusterPivoted); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for state TSClusterPivoted")
			return ctrl.Result{}, err
		}
		if err := r.setAnnotation(ctx, clusterDef, clusterUrlAnnotation, cache.ServerURL); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for cluster-url")
			return ctrl.Result{}, err
		}
		if err := r.setAnnotation(ctx, clusterDef, clusterPivotedAnnotation, "true"); err != nil {
			log.Error(err, "Cannot set annotation on clusterdef for cluster-pivoted")
			return ctrl.Result{}, err
		}

	}
	return ctrl.Result{RequeueAfter: PivotMilliResyncPeriod * time.Millisecond}, nil
}

func (r *ClusterDefReconciler) isClusterReadyForPivot(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	log logr.Logger,
) (error, bool) {

	var machines capi.MachineList

	isReady := true

	err := r.Client.List(ctx, &machines, client.MatchingLabels{"cluster.x-k8s.io/cluster-name": clusterDef.Name})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "Cannot list machines in cluster.")
			return err, false
		}
	}

	for _, machine := range machines.Items {
		if machine.Status.Phase != "Running" {
			log.Info("machine not in running phase", "machine", machine.Name)
			isReady = false
			return nil, isReady
		}
		for _, condition := range machine.Status.Conditions {
			if condition.Type == capi.ReadyCondition && condition.Status != corev1.ConditionTrue {
				log.Info("machine not in ready state", "machine", machine.Name)
				isReady = false
				return nil, isReady
			}
		}
	}
	return nil, isReady
}

// getTargetClusterNamespace return the namespace of the target cluster
func (r *ClusterDefReconciler) getTargetClusterNamespace(
	clusterDef *gitopsv1.ClusterDef,
) string {
	if clusterDef.Spec.MultiTenant {
		return fmt.Sprintf("%s-%s", clusterDef.Namespace, clusterDef.Name)
	} else {
		return clusterDef.Namespace

	}
}

// getClusterdefNamespace compute the namespace of the clusterdef resource
// from the namespace of the cluster
func (r *ClusterDefReconciler) getClusterdefNamespace(
	clusterNamespace string,
	clusterName string,
) string {
	return strings.TrimSuffix(clusterNamespace, fmt.Sprintf("-%s", clusterName))
}

func (r *ClusterDefReconciler) getKubeConfig(ctx context.Context, clusterName string, clusterNamespace string) (*clientapi.Config, error) {
	secret := &corev1.Secret{}
	clusterSecretName := fmt.Sprintf("%s-kubeconfig", clusterName)
	key := client.ObjectKey{
		Namespace: clusterNamespace,
		Name:      clusterSecretName,
	}
	err := r.Get(ctx, key, secret)
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			r.Log.Error(err, fmt.Sprintf("failed to get cluster kubeconfig secret %s in namespace %s", clusterSecretName, clusterNamespace))
			return nil, err
		} else {
			return nil, nil
		}
	} else {
		r.Log.Info("kubeconfig for cluster retrieved", "clusterName", clusterName)
		if secret.Data == nil {
			r.Log.Error(err, "no data in kubeconfig secret", "clusterSecretName", clusterSecretName, "clusterNamespace", clusterNamespace)
		}
		secretData, presence := secret.Data["value"]
		if !presence {
			r.Log.Error(err, "unable to find kubeconfig info in secret", "clusterSecretName", clusterSecretName)
			return nil, err
		}
		kubeconfig, err := clientcmd.Load(secretData)
		if err != nil {
			r.Log.Error(err, "unable to load kubeconfig", "clusterSecretName", clusterSecretName)
			return nil, err
		}

		return kubeconfig, nil
	}
}
