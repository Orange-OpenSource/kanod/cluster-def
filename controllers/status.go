/*
Copyright 2022 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	kcpkamajiv1alpha1 "github.com/clastix/cluster-api-control-plane-provider-kamaji/api/v1alpha1"
	"github.com/go-logr/logr"
	corev1 "k8s.io/api/core/v1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/sets"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"
	capi "sigs.k8s.io/cluster-api/api/v1beta1"
	cpcapi "sigs.k8s.io/cluster-api/controlplane/kubeadm/api/v1beta1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/reconcile"

	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	argocd "gitlab.com/Orange-OpenSource/kanod/cluster-def/thirdparty/argocd/apis/v1alpha1"
)

// UpdateConditions method update the status of a clusterDef resource
func (r *ClusterDefReconciler) UpdateStatus(
	nsName types.NamespacedName,
	phase gitopsv1.ClusterDefState,
	cpVersion string,
	versions []string,
	pivotStatus gitopsv1.PivotState,
) error {
	var clusterDef gitopsv1.ClusterDef
	log := r.Log.WithValues("clusterdef", nsName.Name)
	ctx := context.Background()
	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := r.Get(ctx, nsName, &clusterDef); err != nil {
				return err
			}
			modified := false
			if clusterDef.Status.PivotStatus != pivotStatus {
				clusterDef.Status.PivotStatus = pivotStatus
				modified = true
			}

			if phase != clusterDef.Status.Phase {
				clusterDef.Status.Phase = phase
				modified = true
			}
			if (phase == gitopsv1.Ready || phase == gitopsv1.Running) && cpVersion != clusterDef.Status.ControlPlaneVersion {
				clusterDef.Status.ControlPlaneVersion = cpVersion
				modified = true
			}
			if (phase == gitopsv1.Ready || phase == gitopsv1.Running) && !sliceEqual(versions, clusterDef.Status.MachineVersions) {
				clusterDef.Status.MachineVersions = versions
				modified = true
			}
			if modified {
				log.Info("Updating status clusterdef", "phase", phase)
				return r.Client.Status().Update(ctx, &clusterDef)
			}
			return nil
		})
	if err != nil {
		log.Error(err, "Cannot update status.")
	}
	return err
}

// UpdateConditions method update the status of a clusterDef resource
func (r *ClusterDefReconciler) UpdatePivotStatus(
	nsName types.NamespacedName,
	pivotStatus gitopsv1.PivotState,
) error {
	var clusterDef gitopsv1.ClusterDef
	log := r.Log.WithValues("clusterdef", nsName.Name)
	ctx := context.Background()
	err := retry.RetryOnConflict(retry.DefaultRetry,
		func() error {
			if err := r.Get(ctx, nsName, &clusterDef); err != nil {
				return err
			}
			modified := false
			if clusterDef.Status.PivotStatus != pivotStatus {
				clusterDef.Status.PivotStatus = pivotStatus
				modified = true
			}

			if modified {
				log.Info("Updating status clusterdef", "pivotStatus", pivotStatus)
				return r.Client.Status().Update(ctx, &clusterDef)
			}
			return nil
		})
	if err != nil {
		log.Error(err, "Cannot update status.")
	}
	return err
}

// MapObjectToClusterDef associates the ClusterDef that triggered the creation of
// the cluster that owns this object.
func (r *ClusterDefReconciler) MapObjectToClusterDef(ctx context.Context, object client.Object) []reconcile.Request {
	clusterName := object.GetLabels()["cluster.x-k8s.io/cluster-name"]
	if clusterName == "" {
		return nil
	} else {
		return []reconcile.Request{
			{NamespacedName: types.NamespacedName{
				Name:      clusterName,
				Namespace: r.getClusterdefNamespace(object.GetNamespace(), clusterName),
			}},
		}
	}
}

/*
computeControlPlaneVersion retrieve the current version of the control-plane and the cluster is ready or not

	of the cluster as entered in the status of the kubeadmcontrolplane associated
	to the cluster
*/
func (r *ClusterDefReconciler) computeControlPlaneVersion(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	log logr.Logger,
	cache *ClusterDefStateCache,
) (string, bool) {
	var version = ""
	var isReady bool
	var err error
	var kubeadmControlplane cpcapi.KubeadmControlPlane
	var kamajiControlplane kcpkamajiv1alpha1.KamajiControlPlane
	var cluster capi.Cluster

	targetClusterNamespace := r.getTargetClusterNamespace(clusterDef)

	// Get controlplane kind in cluster resource
	if cache.IsClusterPivoted {
		restClient := getClientWithKubeconfig(cache.Kubeconfig, &capi.GroupVersion)
		err = restClient.Get().Namespace(targetClusterNamespace).Resource("clusters").Name(clusterDef.Name).Do(context.TODO()).Into(&cluster)
	} else {
		key := types.NamespacedName{
			Name:      clusterDef.Name,
			Namespace: targetClusterNamespace,
		}
		err = r.Client.Get(ctx, key, &cluster)
	}
	if err != nil {
		log.Error(err, "Error while getting cluster info")
	}
	kindControlPlane := cluster.Spec.ControlPlaneRef.Kind
	groupVersion := cpcapi.GroupVersion
	log.Info("computeControlPlaneVersion with ", "kindControlPlane", kindControlPlane)
	if kindControlPlane == "KamajiControlPlane" {
		groupVersion = kcpkamajiv1alpha1.GroupVersion
	}

	// Get controlplane resource
	if cache.IsClusterPivoted {
		cpName := fmt.Sprintf("controlplane-%s", clusterDef.Name)
		if kindControlPlane == "KamajiControlPlane" {
			restClient := getClientWithKubeconfig(cache.Kubeconfig, &groupVersion)
			err = restClient.Get().Namespace(targetClusterNamespace).Resource("KamajiControlPlanes").Name(cpName).Do(context.TODO()).Into(&kamajiControlplane)
		} else {
			restClient := getClientWithKubeconfig(cache.Kubeconfig, &groupVersion)
			err = restClient.Get().Namespace(targetClusterNamespace).Resource("KubeadmControlPlanes").Name(cpName).Do(context.TODO()).Into(&kubeadmControlplane)
		}
	} else {
		key := types.NamespacedName{
			Name:      fmt.Sprintf("controlplane-%s", clusterDef.Name),
			Namespace: targetClusterNamespace,
		}
		if kindControlPlane == "KamajiControlPlane" {
			err = r.Client.Get(ctx, key, &kamajiControlplane)
		} else {
			err = r.Client.Get(ctx, key, &kubeadmControlplane)
		}
	}

	// Get status infos
	if err != nil {
		log.Error(err, "Error while getting controlplane info")
	} else {
		if kindControlPlane == "KamajiControlPlane" {
			if kamajiControlplane.Status.Version != "" {
				version = kamajiControlplane.Status.Version
				isReady = kamajiControlplane.Status.Ready
			}
		} else {
			if kubeadmControlplane.Status.Version != nil {
				version = *kubeadmControlplane.Status.Version
				isReady = kubeadmControlplane.Status.Ready
			}
		}
		log.Info("ControlPlane Status", "version", version, "isReady", isReady)
	}
	return version, isReady
}

/* computeStatus computes how far the cluster has been rendered on the target
   servers.

   It also gives back the current version of the control plane and the versions
   of all the kubelet associated to the cluster. */

func (r *ClusterDefReconciler) computeStatus(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	log logr.Logger,
	cache *ClusterDefStateCache,
) (gitopsv1.ClusterDefState, string, []string) {
	var versions = []string{}
	var cpVersion = ""
	var cpReady bool
	var phase = gitopsv1.Failed
	var app argocd.Application
	var err error
	var restClient *rest.RESTClient

	targetClusterNamespace := r.getTargetClusterNamespace(clusterDef)

	key := types.NamespacedName{
		Name:      fmt.Sprintf("cdef-%s", clusterDef.Name),
		Namespace: r.Config.ArgoCDNamespace,
	}
	if err = r.Client.Get(ctx, key, &app); err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("Cannot get application yet")
		} else {
			log.Error(err, "Cannot get application")
		}

		return phase, cpVersion, versions
	}
	phase = gitopsv1.ApplicationCreated

	if app.Status.Sync.Status != argocd.SyncStatusCodeSynced {
		return phase, cpVersion, versions
	}
	phase = gitopsv1.ApplicationSynced

	if cache.IsClusterPivoted {
		restClient = getClientWithKubeconfig(cache.Kubeconfig, &capi.GroupVersion)
	}
	var cluster capi.Cluster
	if cache.IsClusterPivoted {
		err = restClient.Get().Namespace(targetClusterNamespace).Resource("clusters").Name(clusterDef.Name).Do(context.TODO()).Into(&cluster)
		if err != nil {
			log.Error(err, "Cannot get Cluster with restClient")
		}
	} else {
		key = types.NamespacedName{
			Name:      clusterDef.Name,
			Namespace: targetClusterNamespace,
		}
		err = r.Client.Get(ctx, key, &cluster)
	}
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("Cannot get cluster yet")
		} else {
			log.Error(err, "Cannot get Cluster.")
		}
		return phase, cpVersion, versions
	}

	if cluster.Status.Phase != "Provisioned" {
		return phase, cpVersion, versions
	}
	phase = gitopsv1.ClusterProvisionned

	var machines capi.MachineList
	if cache.IsClusterPivoted {
		labelSelect := metav1.ListOptions{
			LabelSelector: fmt.Sprintf("cluster.x-k8s.io/cluster-name=%s", clusterDef.Name),
		}

		err = restClient.Get().Namespace(targetClusterNamespace).
			Resource("machines").VersionedParams(&labelSelect, runtime.NewParameterCodec(clientgoscheme.Scheme)).
			Do(context.TODO()).Into(&machines)
	} else {
		err = r.Client.List(ctx, &machines, client.MatchingLabels{"cluster.x-k8s.io/cluster-name": clusterDef.Name})
	}
	if err != nil {
		log.Error(err, "Cannot list machines.")
		return phase, cpVersion, versions
	}

	cpVersion, cpReady = r.computeControlPlaneVersion(ctx, clusterDef, log, cache)

	versionSet := sets.NewString()
	isOneMachineNotRunning := false
	for _, machine := range machines.Items {

		if machine.Status.Phase != "Running" {
			isOneMachineNotRunning = true
			continue
		}
		for _, condition := range machine.Status.Conditions {
			if condition.Type == capi.ReadyCondition && condition.Status == corev1.ConditionTrue {
				phase = gitopsv1.Running
			}
		}
		versionSet.Insert(machine.Status.NodeInfo.KubeletVersion)
	}

	if cpReady && isOneMachineNotRunning {
		phase = gitopsv1.Ready
	}

	versions = versionSet.List()
	return phase, cpVersion, versions
}

/* needResync checks if the application should be resynced. */
func needResync(
	clusterDef *gitopsv1.ClusterDef,
	cpVersion string,
	kubeletVersions []string,
	log logr.Logger,
) bool {
	annotations := clusterDef.GetAnnotations()
	limitCp, present := annotations["kanod.io/bound-controlplane"]
	if present && len(kubeletVersions) > 0 {
		// The array is sorted, the first element is the lowest kubelet version
		// It should match the minor of the limit on control plane so that we
		// can increase the control plane minor of at least one unit
		minKb := kubeletVersions[0]
		bound := fmt.Sprintf("v1.%s", limitCp)
		log.Info(
			"Try to remove controlplane bound",
			"minKubelet", minKb,
			"controlPlaneBound", bound)
		if minKb > bound {
			return true
		}
	}
	limitWorkers, present := annotations["kanod.io/bound-workers"]
	if present && fmt.Sprintf("v1.%sX", limitWorkers) < cpVersion {
		log.Info(
			"Remove worker bound", "oldBound", limitWorkers,
			"newBound", cpVersion)
		return true
	}
	return false
}

/* syncApp annotates the argocd application to force a resync */
func (r *ClusterDefReconciler) syncApp(
	ctx context.Context, cdefName *types.NamespacedName,
	log logr.Logger,
) error {
	log.Info("Force refresh on application")
	patch := []byte(`{"metadata":{"annotations":{"argocd.argoproj.io/refresh": "hard"}}}`)
	err := r.Client.Patch(
		ctx,
		&argocd.Application{
			ObjectMeta: metav1.ObjectMeta{
				Name:      fmt.Sprintf("cdef-%s", cdefName.Name),
				Namespace: r.Config.ArgoCDNamespace,
			},
		},
		client.RawPatch(types.MergePatchType, patch),
	)
	if err != nil {
		log.Error(err, "Cannot patch the associated application.")
	}
	return err
}
