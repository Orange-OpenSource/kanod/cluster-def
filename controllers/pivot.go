/*
Copyright 2022 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"os"

	appsv1 "k8s.io/api/apps/v1"

	"github.com/go-logr/logr"
	"github.com/pkg/errors"
	gitopsv1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/api/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	ctrl "sigs.k8s.io/controller-runtime"

	bmpv1 "gitlab.com/Orange-OpenSource/kanod/baremetalpool/api/v1"
	byohv1beta1 "gitlab.com/Orange-OpenSource/kanod/cluster-def/byoh_api/v1beta1"

	bmhv1alpha1 "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	k8serrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/util/retry"
	clctl "sigs.k8s.io/cluster-api/cmd/clusterctl/client"
	logf "sigs.k8s.io/cluster-api/cmd/clusterctl/log"
	capiversion "sigs.k8s.io/cluster-api/version"

	//	clientgo "k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
	clientapi "k8s.io/client-go/tools/clientcmd/api"
	capi "sigs.k8s.io/cluster-api/api/v1beta1"
	"sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/controller-runtime/pkg/controller/controllerutil"

	// kanodv1 "gitlab.com/Orange-OpenSource/kanod/kanod-operator/api/v1"
	m3ipam "github.com/metal3-io/ip-address-manager/api/v1alpha1"
	keav1alpha1 "gitlab.com/Orange-OpenSource/kanod/kanod-kea/api/v1alpha1"
)

func (r *ClusterDefReconciler) StoreTargetClusterInfo(
	ctx context.Context,
	cache *ClusterDefStateCache,
	clusterDef *gitopsv1.ClusterDef,
	log logr.Logger,
) error {
	var cluster capi.Cluster

	key := types.NamespacedName{
		Name:      cache.ClusterName,
		Namespace: cache.ClusterNamespace,
	}
	if err := r.Client.Get(ctx, key, &cluster); err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("Cannot store target cluster info, cannot get cluster yet")
		} else {
			log.Error(err, "Cannot get Cluster.")
		}
		return err
	}
	cache.ClusterName = cluster.GetName()
	cache.ClusterNamespace = cluster.GetNamespace()

	secret := &corev1.Secret{}
	key = client.ObjectKey{
		Namespace: cache.ClusterNamespace,
		Name:      fmt.Sprintf("%s-kubeconfig", cache.ClusterName),
	}
	if err := r.Get(ctx, key, secret); err != nil {
		log.V(0).Info("unable to get Kubeconfig secret yet")
		return err
	} else {
		kcData, presence := secret.Data["value"]
		if !presence {
			log.Error(err, "unable to find kubeconfig info")
			return err
		}
		log.V(0).Info("Found kubeconfig")
		if config, err := clientcmd.Load(kcData); err != nil {
			log.Error(err, "unable to internalize kubeconfig info")
		} else {
			cache.Kubeconfig = config
		}
	}
	kubeConfig := cache.Kubeconfig
	currentContext, presence := kubeConfig.Contexts[kubeConfig.CurrentContext]
	if !presence {
		return errors.New("cannot find current context")
	}
	server, presence := kubeConfig.Clusters[currentContext.Cluster]
	if !presence {
		return errors.New("cannot find current server")
	}
	cache.ServerURL = server.Server

	return nil
}

// Create in the cluster the secret for ArgoCD
func (r *ClusterDefReconciler) UpdateArgoCDConfig(
	ctx context.Context,
	clusterName string,
	kubeConfig *clientapi.Config,
) error {

	// Create the secret containing target cluster configuration.
	secret, _, err := r.createArgoTargetConfig(clusterName, kubeConfig)
	if err != nil {
		return fmt.Errorf("cannot generate argocd secret for target cluster: %w", err)
	}

	r.Log.Info("Creating argocd secret for target cluster")
	if _, err := controllerutil.CreateOrUpdate(ctx, r.Client, secret, func() error { return nil }); err != nil {
		return fmt.Errorf("cannot create argocd secret on target cluster: %w", err)
	}

	return nil
}

// Create the secret containing the KubeConfig of target cluster for ArgoCD
func (r *ClusterDefReconciler) createArgoTargetConfig(
	clusterName string, // name of the target cluster
	kubeconfig *clientapi.Config, // kubeconfig of the client
) (*corev1.Secret, string, error) {
	r.Log.Info("create argocd secret for cluster", "clusterName", clusterName)
	if kubeconfig.Contexts == nil {
		return nil, "", errors.New("missing Contexts field in kubeconfig")
	}
	currentContext, presence := kubeconfig.Contexts[kubeconfig.CurrentContext]
	if !presence {
		return nil, "", errors.New("cannot find current context")
	}

	if kubeconfig.AuthInfos == nil {
		return nil, "", errors.New("missing AuthInfos field in kubeconfig")
	}
	user, presence := kubeconfig.AuthInfos[currentContext.AuthInfo]
	if !presence {
		return nil, "", errors.New("cannot find current user infos")
	}

	if kubeconfig.Clusters == nil {
		return nil, "", errors.New("missing Clusters field in kubeconfig")
	}
	server, presence := kubeconfig.Clusters[currentContext.Cluster]
	if !presence {
		return nil, "", errors.New("cannot find current server")
	}
	if server.CertificateAuthorityData == nil {
		return nil, "", errors.New("cannot find CA")
	}
	if user.ClientKeyData == nil {
		return nil, "", errors.New("cannot find user key")
	}
	if user.ClientCertificateData == nil {
		return nil, "", errors.New("cannot find user certificate")
	}
	caData := base64.StdEncoding.EncodeToString(server.CertificateAuthorityData)
	keyData := base64.StdEncoding.EncodeToString(user.ClientKeyData)
	certData := base64.StdEncoding.EncodeToString(user.ClientCertificateData)
	config := fmt.Sprintf(
		"{\"tlsClientConfig\":{\"insecure\":false,\"certData\":\"%s\",\"keyData\":\"%s\",\"caData\":\"%s\"}}",
		certData, keyData, caData)
	data := make(map[string][]byte)
	data["config"] = []byte(config)
	data["server"] = []byte(server.Server)
	data["name"] = []byte(clusterName)

	secret := &corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Name:      fmt.Sprintf("%s-config", clusterName),
			Namespace: r.Config.ArgoCDNamespace,
			Labels: map[string]string{
				"argocd.argoproj.io/secret-type": "cluster",
			},
		},
		Data: data,
	}
	return secret, server.Server, nil
}

// Label/unlabel the resources for the clusterctl move process
func (r *ClusterDefReconciler) labelResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	log logr.Logger,
	addLabelFlag bool,
) error {
	restConfig := ctrl.GetConfigOrDie()
	dynClient, err := dynamic.NewForConfig(restConfig)
	if err != nil {
		log.Error(err, "Problem while creating cluster config.")
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "apiextensions.k8s.io",
		Version:  "v1",
		Resource: "customresourcedefinitions",
	}

	options := metav1.ListOptions{
		LabelSelector: "cluster.x-k8s.io/provider",
		Limit:         50,
	}

	rsc, err := dynClient.Resource(gvr).List(ctx, options)
	if err != nil {
		log.Error(err, "Problem while listing resources.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "baremetalhosts.metal3.io", "clusterctl.cluster.x-k8s.io", "", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling baremetalhosts.metal3.io CRD resource.")
		return err
	}

	// add label on unused baremetalhosts in order to move them
	if err = labelRsrc(dynClient, ctx, log, gvr, "baremetalhosts.metal3.io", "clusterctl.cluster.x-k8s.io/move-hierarchy", "", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling baremetalhosts.metal3.io CRD resource.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "hosts.kanod.io", "clusterctl.cluster.x-k8s.io", "", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling hosts.kanod.io CRD resource.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "hardwaredata.metal3.io", "clusterctl.cluster.x-k8s.io", "", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling baremetalhosts.metal3.io CRD resource.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "hardwaredata.metal3.io", "clusterctl.cluster.x-k8s.io/move", "", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling hardwaredata.metal3.io CRD resource.")
		return err
	}

	if err = labelRsrc(dynClient, ctx, log, gvr, "ippools.ipam.metal3.io", "clusterctl.cluster.x-k8s.io/move-hierarchy", "", addLabelFlag); err != nil {
		log.Error(err, "Problem while labeling ippools.ipam.metal3.io CRD resource.")
		return err
	}

	if clusterDef.Spec.PivotInfo.ByoHostPoolUsed {
		byohResourcesList := [5]string{
			"byoclusters.infrastructure.cluster.x-k8s.io",
			"byoclustertemplates.infrastructure.cluster.x-k8s.io",
			"byohosts.infrastructure.cluster.x-k8s.io",
			"byomachines.infrastructure.cluster.x-k8s.io",
			"byomachinetemplates.infrastructure.cluster.x-k8s.io",
		}
		for _, crd := range byohResourcesList {
			if err = labelRsrc(dynClient, ctx, log, gvr, crd, "clusterctl.cluster.x-k8s.io/move-hierarchy", "", addLabelFlag); err != nil {
				log.Error(err, fmt.Sprintf("Problem while labeling %s CRD resource.", crd))
				return err
			}
		}
	}

	for _, crd := range rsc.Items {
		metadata := crd.Object["metadata"].(map[string]interface{})
		crdName := fmt.Sprintf("%v", metadata["name"])

		if err = labelRsrc(dynClient, ctx, log, gvr, crdName, "clusterctl.cluster.x-k8s.io", "", addLabelFlag); err != nil {
			log.Error(err, "Problem while labeling resource resource.")
			return err

		}
	}
	return nil
}

// Label/unlabel a specific resource
func labelRsrc(dynclient dynamic.Interface,
	ctx context.Context,
	log logr.Logger,
	gvr schema.GroupVersionResource,
	name string,
	label string,
	value string,
	addLabelFlag bool,
) error {
	result, err := dynclient.Resource(gvr).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			log.Info("resource missing for labeling process, ignoring it.", "name", name)
			return nil
		} else {
			log.Error(err, "Problem while getting resource.")
			return err
		}
	}

	var tmp map[string]string
	tmp = result.GetLabels()
	if addLabelFlag {
		log.Info(fmt.Sprintf("Adding label %s on resource %s", label, name))
		if tmp == nil {
			tmp = map[string]string{label: value}
		} else {
			tmp[label] = value
		}
	} else {
		log.Info(fmt.Sprintf("Deleting label %s on resource %s", label, name))
		delete(tmp, label)
	}

	result.SetLabels(tmp)
	_, err = dynclient.Resource(gvr).Update(ctx, result, metav1.UpdateOptions{})
	if err != nil {
		log.Error(err, "Problem while updating resource.")
		return err
	}
	return nil
}

// move labelled resources to the target cluster with clusterctl api
func (r *ClusterDefReconciler) moveClusterResources(
	ctx context.Context,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfig, _ := rest.InClusterConfig()
	config := generateKubeconfigFromRestconfig(restConfig)

	fKcLocal, err := os.CreateTemp("", "kc-local")
	if err != nil {
		log.Error(err, "Problem while creating temp file.")
		return err
	}

	defer os.Remove(fKcLocal.Name())

	fKcTarget, err := os.CreateTemp("", "kc-target")
	if err != nil {
		log.Error(err, "Problem while creating temp file.")
		return err
	}

	defer os.Remove(fKcTarget.Name())

	err = clientcmd.WriteToFile(*config, fKcLocal.Name())
	if err != nil {
		log.Error(err, "Problem while writting local kubeconfig file.")
		return err
	}

	configTarget := cache.Kubeconfig

	err = clientcmd.WriteToFile(*configTarget, fKcTarget.Name())
	if err != nil {
		log.Error(err, "Problem while writting target kubeconfig file.")
		return err
	}

	c, err := clctl.New(ctx, "")
	if err != nil {
		log.Error(err, "Problem while creating clusterctl client.")
		return err
	}
	verbosity := 5
	logf.SetLogger(logf.NewLogger(logf.WithThreshold(&verbosity)))
	namespace := cache.ClusterNamespace

	log.Info(fmt.Sprintf("clusterapi version : %s", capiversion.Get().GitVersion))
	err = c.Move(ctx, clctl.MoveOptions{
		FromKubeconfig: clctl.Kubeconfig{Path: fKcLocal.Name(), Context: ""},
		ToKubeconfig:   clctl.Kubeconfig{Path: fKcTarget.Name(), Context: ""},
		Namespace:      namespace,
		DryRun:         false,
	})

	if err != nil {
		log.Error(err, "Problem while moving resources.")
		return err
	}

	return nil
}

// Move baremetalpool resources listed in the clusterdef spec to the target cluster
func (r *ClusterDefReconciler) moveBareMetalPoolResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrbmp := schema.GroupVersionResource{
		Group:    "bmp.kanod.io",
		Version:  "v1",
		Resource: "baremetalpools",
	}

	gvrsecret := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "secrets",
	}

	gvrcmap := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "configmaps",
	}

	bmpNamespace := cache.ClusterNamespace

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		bmp, err := dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
		if err == nil {
			spec := bmp.Object["spec"].(map[string]interface{})
			credentialName := fmt.Sprintf("%v", spec["credentialName"])
			brokerConfig := fmt.Sprintf("%v", spec["brokerConfig"])

			log.Info(fmt.Sprintf("Prepare moving baremetalpool resource %s with secret %s", bmpName, credentialName))

			// Move BareMetalPool secret
			// get secret associated with the baremetalpool on local cluster
			secret, err := dynClientLocal.Resource(gvrsecret).Namespace(bmpNamespace).Get(ctx, credentialName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting baremetalpool secret on local cluster")
				return err
			}

			// create secret associated with the baremetalpool on target cluster
			log.Info(fmt.Sprintf("creating secret %s on target cluster", credentialName))
			secret.SetResourceVersion("")
			_, err = dynClientTarget.Resource(gvrsecret).Namespace(bmpNamespace).Create(ctx, secret, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating baremetalpool secret on target cluster")
				return err
			}

			// Move BareMetalPool configmap
			// get configmap associated with the baremetalpool on local cluster
			cmap, err := dynClientLocal.Resource(gvrcmap).Namespace(bmpNamespace).Get(ctx, brokerConfig, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting baremetalpool configmap on local cluster")
				return err
			}

			// create configmap associated with the baremetalpool on target cluster
			log.Info(fmt.Sprintf("creating configmap %s on target cluster", brokerConfig))
			cmap.SetResourceVersion("")
			_, err = dynClientTarget.Resource(gvrcmap).Namespace(bmpNamespace).Create(ctx, cmap, metav1.CreateOptions{})
			if err != nil {
				if !k8serrors.IsAlreadyExists(err) {
					log.Error(err, "error while creating baremetalpool configmap on target cluster")
					return err
				} else {
					log.Info(fmt.Sprintf("baremetalpool configmap %s already exists on target cluster, ignoring creation", brokerConfig))
				}
			}

			// remove baremetalpool on local cluster and create baremetalpool on target cluster
			// remove pause annotation on baremetalpool
			annotations := bmp.GetAnnotations()
			delete(annotations, bmpAnnotationPaused)
			bmp.SetAnnotations(annotations)

			log.Info(fmt.Sprintf("deleting baremetalpool %s on local cluster", bmpName))
			err = dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Delete(ctx, bmpName, metav1.DeleteOptions{})
			if err != nil {
				log.Error(err, "error while deleting baremetalpool resource on local cluster")
				return err
			}

			bmp.SetResourceVersion("")
			log.Info(fmt.Sprintf("creating baremetalpool %s on target cluster", bmpName))
			_, err = dynClientTarget.Resource(gvrbmp).Namespace(bmpNamespace).Create(ctx, bmp, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating baremetalpool resource on target cluster")
				return err
			}

		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting baremetalpool on local cluster")
				return err
			} else {
				log.Error(err, fmt.Sprintf("baremetalpool %s missing in local cluster", bmpName))
				return err
			}
		}
	}
	return nil
}

// Move byohostpool resources to the target cluster
func (r *ClusterDefReconciler) moveByoHostPoolResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrbhpscaler := schema.GroupVersionResource{
		Group:    "pools.kanod.io",
		Version:  "v1alpha1",
		Resource: "byohostpoolscalers",
	}

	apiServer := cache.ServerURL

	bhpScalerName := cache.ClusterName
	bhpScalerNamespace := cache.ClusterNamespace

	bhpscaler, err := dynClientLocal.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Get(ctx, bhpScalerName, metav1.GetOptions{})
	if err == nil {
		// remove byohostpoolscaler on local cluster and create byohostpoolscaler on target cluster
		// remove pause annotation on byohostpoolscaler
		annotations := bhpscaler.GetAnnotations()
		delete(annotations, bhpscalerAnnotationPaused)
		annotations[apiServerByoHostPoolScalerAnnotation] = apiServer
		bhpscaler.SetAnnotations(annotations)
		bhpscaler.SetResourceVersion("")

		log.Info(fmt.Sprintf("updating byohostpoolscaler %s for target cluster with apiServer %s", bhpScalerName, apiServer))
		// unstructured.SetNestedField(bhpscaler.Object, apiServer, "spec", "apiServer")

		log.Info(fmt.Sprintf("creating byohostpoolscaler %s on target cluster", bhpScalerName))
		_, err = dynClientTarget.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Create(ctx, bhpscaler, metav1.CreateOptions{})
		if err != nil {
			log.Error(err, "error while creating byohostpoolscaler resource on target cluster")
			return err
		}

		log.Info(fmt.Sprintf("deleting byohostpoolscaler %s on local cluster", bhpScalerName))
		err = dynClientLocal.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Delete(ctx, bhpScalerName, metav1.DeleteOptions{})
		if err != nil {
			log.Error(err, "error while deleting byohostpoolscaler resource on local cluster")
			return err
		}

	} else {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting byohostpoolscaler on local cluster")
			return err
		} else {
			log.Info(fmt.Sprintf("no byohostpoolscaler %s in local cluster, ignoring", bhpScalerName))
			return nil
		}
	}

	gvrSecrets := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "secrets",
	}

	bmpNamespace := cache.ClusterNamespace

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		secretUserdataName := fmt.Sprintf("%s-userdata", bmpName)
		secretUserdata, err := dynClientLocal.Resource(gvrSecrets).Namespace(bmpNamespace).Get(ctx, secretUserdataName, metav1.GetOptions{})
		if err == nil {
			log.Info(fmt.Sprintf("Prepare moving userdata secret resource %s", secretUserdataName))

			// create userdata secret on target cluster
			log.Info(fmt.Sprintf("creating secret %s on target cluster", secretUserdataName))
			secretUserdata.SetResourceVersion("")
			_, err = dynClientTarget.Resource(gvrSecrets).Namespace(bmpNamespace).Create(ctx, secretUserdata, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating secret on target cluster", "name", secretUserdataName)
				return err
			}

			log.Info(fmt.Sprintf("deleting secret %s on local cluster", secretUserdataName))
			err = dynClientLocal.Resource(gvrSecrets).Namespace(bmpNamespace).Delete(ctx, secretUserdataName, metav1.DeleteOptions{})
			if err != nil {
				log.Error(err, "error while deleting secret on local cluster")
				return err
			}

		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting secret on local cluster", "name", secretUserdataName)
				return err
			} else {
				log.Info(fmt.Sprintf("secret %s missing in local cluster, ignoring", secretUserdataName))
			}
		}
	}

	return nil
}

// Move DhcpConfig resources listed in the clusterdef spec to the target cluster
func (r *ClusterDefReconciler) moveDhcpConfigResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrDhcpConfig := schema.GroupVersionResource{
		Group:    "network.kanod.io",
		Version:  "v1alpha1",
		Resource: "dhcpconfigs",
	}

	gvrNetwork := schema.GroupVersionResource{
		Group:    "netpool.kanod.io",
		Version:  "v1",
		Resource: "networks",
	}

	networkToUid := make(map[string]types.UID)

	networkNamespace := cache.ClusterNamespace

	for _, nwkName := range clusterDef.Spec.PivotInfo.NetworkList {
		network, err := dynClientLocal.Resource(gvrNetwork).Namespace(networkNamespace).Get(ctx, nwkName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting network on local cluster")
				return err
			} else {
				log.Info(fmt.Sprintf("network %s missing in local cluster", nwkName))
				return err
			}
		} else {
			networkToUid[network.GetName()] = network.GetUID()
		}
	}

	dhcpConfigNamespace := cache.ClusterNamespace

	for _, dhcpConfigName := range clusterDef.Spec.PivotInfo.DhcpConfigList {
		dhcpConfig, err := dynClientLocal.Resource(gvrDhcpConfig).Namespace(dhcpConfigNamespace).Get(ctx, dhcpConfigName, metav1.GetOptions{})
		if err == nil {
			log.Info(fmt.Sprintf("Prepare moving dhcpConfig resource %s", dhcpConfigName))

			dhcpConfig.SetResourceVersion("")

			var emptyMap map[string]interface{}
			if err := unstructured.SetNestedMap(dhcpConfig.Object, emptyMap, "status"); err != nil {
				log.Error(err, "failed to set status in dhcpconfig object", "dhcpConfigNamespace", dhcpConfigNamespace)
				return err
			}

			var newOwnerReferences []metav1.OwnerReference
			owners := dhcpConfig.GetOwnerReferences()
			for _, owner := range owners {
				if owner.Kind == "Network" {
					uid, ok := networkToUid[owner.Name]
					if ok {
						owner.UID = uid
					} else {
						log.Error(err, "network missing for ownerRef in dhcpConfig ", "dhcpConfigName", dhcpConfigName, "netwoprkName", owner.Name)
						return err
					}
				}
				newOwnerReferences = append(newOwnerReferences, owner)
			}

			dhcpConfig.SetOwnerReferences(newOwnerReferences)

			log.Info(fmt.Sprintf("creating dhcpConfig %s on target cluster", dhcpConfigName))
			_, err = dynClientTarget.Resource(gvrDhcpConfig).Namespace(dhcpConfigNamespace).Create(ctx, dhcpConfig, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating dhcpConfig resource on target cluster")
				return err
			}

			log.Info(fmt.Sprintf("deleting dhcpConfig %s on local cluster", dhcpConfigName))
			err = dynClientLocal.Resource(gvrDhcpConfig).Namespace(dhcpConfigNamespace).Delete(ctx, dhcpConfigName, metav1.DeleteOptions{})
			if err != nil {
				if !k8serrors.IsNotFound(err) {
					log.Error(err, "error while deleting dhcpConfig resource on local cluster")
					return err
				} else {
					log.Info(fmt.Sprintf("dhcpConfig %s missing in local cluster, deletion ignored", dhcpConfigName))
					return nil
				}
			}
		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting dhcpConfig on local cluster")
				return err
			} else {
				log.Error(err, fmt.Sprintf("dhcpConfig %s missing in local cluster", dhcpConfigName))
				return err
			}
		}
	}

	return nil
}

// Move network resources listed in the clusterdef spec to the target cluster
func (r *ClusterDefReconciler) moveNetworkResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrnwk := schema.GroupVersionResource{
		Group:    "netpool.kanod.io",
		Version:  "v1",
		Resource: "networks",
	}

	gvrsecret := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "secrets",
	}

	gvrcmap := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "configmaps",
	}

	nwkNamespace := cache.ClusterNamespace

	for _, nwkName := range clusterDef.Spec.PivotInfo.NetworkList {
		network, err := dynClientLocal.Resource(gvrnwk).Namespace(nwkNamespace).Get(ctx, nwkName, metav1.GetOptions{})
		if err == nil {
			spec := network.Object["spec"].(map[string]interface{})
			credentialName := fmt.Sprintf("%v", spec["networkDefinitionCredentialName"])
			brokerConfig := fmt.Sprintf("%v", spec["brokerConfig"])

			log.Info(fmt.Sprintf("Prepare moving network resource %s with networkdefinition secret %s", nwkName, credentialName))

			// Move network secret
			// get secret associated with the network on local cluster
			secret, err := dynClientLocal.Resource(gvrsecret).Namespace(nwkNamespace).Get(ctx, credentialName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting networkdefinition secret on local cluster")
				return err
			}

			// create secret associated with the network on target cluster
			log.Info(fmt.Sprintf("creating networkdefinition secret %s on target cluster", credentialName))
			secret.SetResourceVersion("")
			_, err = dynClientTarget.Resource(gvrsecret).Namespace(nwkNamespace).Create(ctx, secret, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating networkdefinition secret on target cluster")
				return err
			}

			// Move network configmap
			// get configmap associated with the network on local cluster
			cmap, err := dynClientLocal.Resource(gvrcmap).Namespace(nwkNamespace).Get(ctx, brokerConfig, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting network configmap on local cluster")
				return err
			}

			// create configmap associated with the network on target cluster
			log.Info(fmt.Sprintf("creating network configmap %s on target cluster", brokerConfig))
			cmap.SetResourceVersion("")
			_, err = dynClientTarget.Resource(gvrcmap).Namespace(nwkNamespace).Create(ctx, cmap, metav1.CreateOptions{})
			if err != nil {
				if !k8serrors.IsAlreadyExists(err) {
					log.Error(err, "error while creating network configmap on target cluster")
					return err
				} else {
					log.Info(fmt.Sprintf("network configmap %s already exists on target cluster, ignoring creation", brokerConfig))
				}
			}

			log.Info(fmt.Sprintf("deleting network %s on local cluster", nwkName))
			err = dynClientLocal.Resource(gvrnwk).Namespace(nwkNamespace).Delete(ctx, nwkName, metav1.DeleteOptions{})
			if err != nil {
				log.Error(err, "error while deleting network resource on local cluster")
				return err
			}

			network.SetResourceVersion("")
			log.Info(fmt.Sprintf("creating network %s on target cluster", nwkName))
			_, err = dynClientTarget.Resource(gvrnwk).Namespace(nwkNamespace).Create(ctx, network, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, "error while creating network resource on target cluster")
				return err
			}

		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting network on local cluster")
				return err
			} else {
				log.Info(fmt.Sprintf("network %s missing in local cluster, ignoring", nwkName))
			}
		}
	}
	return nil
}

// update clusterName in ippool to ensure correct handling during the move of the resources
func (r *ClusterDefReconciler) updateClusternameInIppool(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) (bool, error) {
	requeue := false
	dhcpConfigNamespace := cache.ClusterNamespace
	for _, dhcpConfigName := range clusterDef.Spec.PivotInfo.DhcpConfigList {
		dhcpConfig := &keav1alpha1.DhcpConfig{}
		key := client.ObjectKey{Namespace: dhcpConfigNamespace, Name: dhcpConfigName}
		if err := r.Client.Get(ctx, key, dhcpConfig); err != nil {
			log.Error(err, "Failed to get dhcpConfig", "name", dhcpConfigName, "namespace", dhcpConfigNamespace)
			return requeue, err
		}

		ippoolName := dhcpConfig.Spec.IPPool
		ippoolNamespace := cache.ClusterNamespace
		if ippoolName != "" {
			ippool := &m3ipam.IPPool{}
			key := client.ObjectKey{Namespace: ippoolNamespace, Name: ippoolName}
			if err := r.Client.Get(ctx, key, ippool); err != nil {
				log.Error(err, "Failed to get ippool", "name", ippoolName, "namespace", ippoolNamespace)
				return requeue, err
			}

			if ippool.Spec.ClusterName == nil || *ippool.Spec.ClusterName != cache.ClusterName {
				ippool.Spec.ClusterName = &cache.ClusterName
				if err2 := r.Client.Update(ctx, ippool); err2 != nil {
					log.Error(err2, "Failed to update ippool", "name", ippoolName, "namespace", ippoolNamespace)
					return requeue, err2
				}
				log.Info(fmt.Sprintf("clusterName spec field updated for ippool %s in local cluster", ippoolName))
				requeue = true
			}
		}
	}

	for _, ippoolName := range clusterDef.Spec.PivotInfo.IppoolList {
		ippoolNamespace := cache.ClusterNamespace
		ippool := &m3ipam.IPPool{}
		key := client.ObjectKey{Namespace: ippoolNamespace, Name: ippoolName}
		if err := r.Client.Get(ctx, key, ippool); err != nil {
			log.Error(err, "Failed to get ippool", "name", ippoolName, "namespace", ippoolNamespace)
			return requeue, err
		}

		if ippool.Spec.ClusterName == nil || *ippool.Spec.ClusterName != cache.ClusterName {
			ippool.Spec.ClusterName = &cache.ClusterName
			if err2 := r.Client.Update(ctx, ippool); err2 != nil {
				log.Error(err2, "Failed to update ippool", "name", ippoolName, "namespace", ippoolNamespace)
				return requeue, err2
			}
			log.Info(fmt.Sprintf("clusterName spec field updated for ippool %s in local cluster", ippoolName))
			requeue = true
		}
	}

	return requeue, nil
}

// unpause baremetalpool resources on target cluster
func (r *ClusterDefReconciler) unpauseBareMetalPoolResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) (error, bool) {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err, false
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvrbmp := schema.GroupVersionResource{
		Group:    "bmp.kanod.io",
		Version:  "v1",
		Resource: "baremetalpools",
	}

	bmpNamespace := cache.ClusterNamespace

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		// check if baremetalpool exists on local cluster
		_, err := dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting baremetalpool on local cluster")
				return err, false
			} else {
				bmpTarget, err := dynClientTarget.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
				if err != nil {
					log.Error(err, "error while getting baremetalpool on target cluster")
					return err, false
				}
				annotations := bmpTarget.GetAnnotations()
				delete(annotations, bmpAnnotationPaused)
				bmpTarget.SetAnnotations(annotations)

				_, err = dynClientTarget.Resource(gvrbmp).Namespace(bmpNamespace).Update(ctx, bmpTarget, metav1.UpdateOptions{})
				if err != nil {
					log.Error(err, "error while updating baremetalpool finalizer on target cluster")
					return err, false
				}
			}
		} else {
			return nil, false
		}
	}
	return nil, true
}

// unpause byohostpoolscaler resources on target cluster
func (r *ClusterDefReconciler) unpauseByoHostResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrByoHost := schema.GroupVersionResource{
		Group:    "infrastructure.cluster.x-k8s.io",
		Version:  "v1beta1",
		Resource: "byohosts",
	}

	gvrByoMachine := schema.GroupVersionResource{
		Group:    "infrastructure.cluster.x-k8s.io",
		Version:  "v1beta1",
		Resource: "byomachines",
	}

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		namespace := cache.ClusterNamespace

		labelSelector := metav1.LabelSelector{MatchLabels: map[string]string{bmpv1.KanodBareMetalPoolNameLabel: bmpName}}

		options := metav1.ListOptions{
			LabelSelector: labels.Set(labelSelector.MatchLabels).String(),
		}
		byoHostList, err := dynClientTarget.Resource(gvrByoHost).Namespace(namespace).List(ctx, options)
		if err != nil {
			log.Error(err, "Problem while listing byohost resources in target cluster")
			return err
		}

		for _, byohost := range byoHostList.Items {
			byoHostName := byohost.GetName()
			byoHostNamespace := byohost.GetNamespace()

			updatedByohost, err := dynClientTarget.Resource(gvrByoHost).Namespace(byoHostNamespace).Get(ctx, byoHostName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while getting byoHost %s  in target cluster", byoHostName))
				return err
			}

			annotations := updatedByohost.GetAnnotations()

			statusAnnot, ok := annotations[byoHostStatusAnnotation]
			if ok {
				var newStatus map[string]interface{}
				log.Info("trying to create status for byohost", "byoHostName", byoHostName)

				json.Unmarshal([]byte(statusAnnot), &newStatus)

				byoMachineName, ok, err := unstructured.NestedString(newStatus, "machineRef", "name")
				if err != nil {
					log.Error(err, "failed to get machineRef in byoHost")
					return err
				}

				if !ok {
					err = fmt.Errorf("missing machineRef in status annotation for byoHost")
					log.Error(err, "failed to get machineRef in byoHost")
					return err
				}

				byoMachine, err := dynClientTarget.Resource(gvrByoMachine).Namespace(byoHostNamespace).Get(ctx, byoMachineName, metav1.GetOptions{})
				if err != nil {
					log.Error(err, "error while getting byomachine")
					return err
				}

				uid := byoMachine.GetUID()
				if err := unstructured.SetNestedField(newStatus, string(uid), "machineRef", "uid"); err != nil {
					log.Error(err, "error while setting uid in in byoHost status object")
					return err
				}

				if err := unstructured.SetNestedMap(byohost.Object, newStatus, "status"); err != nil {
					log.Error(err, "failed to set status in byoHost object")
					return err
				}

				_, err = dynClientTarget.Resource(gvrByoHost).Namespace(byoHostNamespace).UpdateStatus(context.TODO(), &byohost, metav1.UpdateOptions{})
				if err != nil {
					log.Error(err, fmt.Sprintf("error while updating status for byoHost %s in target cluster", byoHostName))
				}

				log.Info(fmt.Sprintf("status created from annotation for byohost %s ", byoHostName))
			}

			log.Info("trying to remove paused annotation from byohost", "byoHostName", byoHostName)

			err = retry.RetryOnConflict(retry.DefaultRetry,
				func() error {
					updatedByohost, err := dynClientTarget.Resource(gvrByoHost).Namespace(byoHostNamespace).Get(ctx, byoHostName, metav1.GetOptions{})
					if err != nil {
						return err
					}

					annotations := updatedByohost.GetAnnotations()
					delete(annotations, capi.PausedAnnotation)
					updatedByohost.SetAnnotations(annotations)

					_, err = dynClientTarget.Resource(gvrByoHost).Namespace(byoHostNamespace).Update(context.TODO(), updatedByohost, metav1.UpdateOptions{})
					if err != nil {
						log.Info("Cannot update byohost, retrying...", "byoHostName", byoHostName)
						return err
					}

					log.Info(fmt.Sprintf("byohost %s updated in target cluster", byoHostName))

					return nil
				})

			if err != nil {
				log.Error(err, "Cannot update byohost", "byoHostName", byoHostName)
				return err
			}

			log.Info(fmt.Sprintf("paused annotation removed from byohost %s ", byoHostName))
		}
	}

	return nil
}

// unpause byohostpoolscaler resources on target cluster
func (r *ClusterDefReconciler) unpauseByoHostPoolResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) (error, bool) {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err, false
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvrbhpscaler := schema.GroupVersionResource{
		Group:    "pools.kanod.io",
		Version:  "v1alpha1",
		Resource: "byohostpoolscalers",
	}

	bhpScalerName := cache.ClusterName
	bhpScalerNamespace := cache.ClusterNamespace

	_, err = dynClientLocal.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Get(ctx, bhpScalerName, metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting byohostpoolscaler in local cluster")
			return err, false
		} else {
			bhpTarget, err := dynClientTarget.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Get(ctx, bhpScalerName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting byohostpoolscaler in target cluster")
				return err, false
			}
			annotations := bhpTarget.GetAnnotations()
			delete(annotations, bhpscalerAnnotationPaused)
			bhpTarget.SetAnnotations(annotations)

			_, err = dynClientTarget.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Update(ctx, bhpTarget, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating byohostpoolscaler finalizer on target cluster")
				return err, false
			}
			log.Info(fmt.Sprintf("byohostpoolscaler resource unpaused : %s  in target cluster", bhpScalerName))

		}
	} else {
		return nil, false
	}

	gvrbhp := schema.GroupVersionResource{
		Group:    "pools.kanod.io",
		Version:  "v1alpha1",
		Resource: "byohostpools",
	}

	bhpRsc, err := dynClientTarget.Resource(gvrbhp).Namespace(bhpScalerNamespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		log.Error(err, "Problem while listing byohostpools resources in target cluster")
		return err, false
	}

	bhpNamespace := cache.ClusterNamespace

	for _, crd := range bhpRsc.Items {
		bhpName := crd.GetName()

		bhp, err := dynClientTarget.Resource(gvrbhp).Namespace(bhpNamespace).Get(ctx, bhpName, metav1.GetOptions{})
		if err == nil {
			annotations := bhp.GetAnnotations()
			delete(annotations, byoHostPoolPausedAnnotation)
			bhp.SetAnnotations(annotations)

			_, err = dynClientTarget.Resource(gvrbhp).Namespace(bhpNamespace).Update(ctx, bhp, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating byohostpool in target cluster")
				return err, false
			}
			log.Info(fmt.Sprintf("byohostpool resource unpaused : %s  in target cluster", bhpName))

		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, fmt.Sprintf("error while getting byohostpool %s  in target cluster", bhpName))
				return err, false
			} else {
				log.Info(fmt.Sprintf("no byohostpool %s in target cluster, ignoring", bhpName))
			}
		}
	}

	gvrbmh := schema.GroupVersionResource{
		Group:    "metal3.io",
		Version:  "v1alpha1",
		Resource: "baremetalhosts",
	}

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		labelSelector := metav1.LabelSelector{MatchLabels: map[string]string{bmpv1.KanodBareMetalPoolNameLabel: bmpName}}

		options := metav1.ListOptions{
			LabelSelector: labels.Set(labelSelector.MatchLabels).String(),
		}
		bmhRsc, err := dynClientTarget.Resource(gvrbmh).Namespace(bhpScalerNamespace).List(ctx, options)
		if err != nil {
			log.Error(err, "Problem while listing baremetalhosts resources in target cluster")
			return err, false
		}

		for _, crd := range bmhRsc.Items {
			bmhName := crd.GetName()
			bmhNamespace := crd.GetNamespace()

			bmh, err := dynClientTarget.Resource(gvrbmh).Namespace(bmhNamespace).Get(ctx, bmhName, metav1.GetOptions{})
			if err == nil {
				annotations := bmh.GetAnnotations()
				delete(annotations, bmhPausedAnnotation)
				bmh.SetAnnotations(annotations)

				_, err = dynClientTarget.Resource(gvrbmh).Namespace(bmhNamespace).Update(ctx, bmh, metav1.UpdateOptions{})
				if err != nil {
					log.Error(err, "error while updating byohostpool in target cluster")
					return err, false
				}
				log.Info(fmt.Sprintf("baremetalhost resource unpaused : %s  in target cluster", bmhName))

			} else {
				if !k8serrors.IsNotFound(err) {
					log.Error(err, fmt.Sprintf("error while getting baremetalhost %s  in target cluster", bmhName))
					return err, false
				} else {
					log.Info(fmt.Sprintf("no baremetalhost %s in target cluster, ignoring", bmhName))
				}
			}
		}
	}

	return nil, true
}

// Add pause annotation on baremetalpool on local cluster
func (r *ClusterDefReconciler) pauseBareMetalPool(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvrbmp := schema.GroupVersionResource{
		Group:    "bmp.kanod.io",
		Version:  "v1",
		Resource: "baremetalpools",
	}

	bmpNamespace := cache.ClusterNamespace

	// annotationPatch := fmt.Sprintf(`[{"op":"add","path":"/metadata/annotations","value":{"%s":""} }]`, bmpAnnotationPaused)

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		bmp, err := dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Get(ctx, bmpName, metav1.GetOptions{})
		if err == nil {
			annotations := bmp.GetAnnotations()
			annotations[bmpAnnotationPaused] = ""
			bmp.SetAnnotations(annotations)

			// remove baremetalpool finalizer in local cluster
			finalizers := bmp.GetFinalizers()
			var newfinalizers []string
			for _, finalizerItem := range finalizers {
				if finalizerItem != baremetalpoolFinalizer {
					newfinalizers = append(newfinalizers, finalizerItem)
				}
			}
			bmp.SetFinalizers(newfinalizers)

			_, err = dynClientLocal.Resource(gvrbmp).Namespace(bmpNamespace).Update(ctx, bmp, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating baremetalpool finalizer on local cluster")
				return err
			}
		} else {
			log.Error(err, fmt.Sprintf("error while getting baremetalpool %s  in local cluster", bmpName))
			return err
		}
	}

	return nil
}

// Add pause annotation on byohostpool-scaler on local cluster
func (r *ClusterDefReconciler) pauseByoHostPool(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvrbhpscaler := schema.GroupVersionResource{
		Group:    "pools.kanod.io",
		Version:  "v1alpha1",
		Resource: "byohostpoolscalers",
	}

	bhpScalerName := cache.ClusterName
	bhpScalerNamespace := cache.ClusterNamespace

	bhp, err := dynClientLocal.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Get(ctx, bhpScalerName, metav1.GetOptions{})
	if err == nil {
		annotations := bhp.GetAnnotations()
		annotations[bhpscalerAnnotationPaused] = ""
		bhp.SetAnnotations(annotations)

		_, err = dynClientLocal.Resource(gvrbhpscaler).Namespace(bhpScalerNamespace).Update(ctx, bhp, metav1.UpdateOptions{})
		if err != nil {
			log.Error(err, "error while updating byohostpoolscaler on local cluster")
			return err
		}
		log.Info(fmt.Sprintf("byohostpoolscaler resource paused : %s  in local cluster", bhpScalerName))

	} else {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, fmt.Sprintf("error while getting byohostpoolscaler %s  in local cluster", bhpScalerName))
			return err
		} else {
			log.Info(fmt.Sprintf("no byohostpoolscaler %s in local cluster, ignoring", bhpScalerName))
		}
	}

	gvrbhp := schema.GroupVersionResource{
		Group:    "pools.kanod.io",
		Version:  "v1alpha1",
		Resource: "byohostpools",
	}

	bhpRsc, err := dynClientLocal.Resource(gvrbhp).Namespace(bhpScalerNamespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		log.Error(err, "Problem while listing byohostpools resources.")
		return err
	}

	bhpNamespace := cache.ClusterNamespace

	for _, crd := range bhpRsc.Items {
		bhpName := crd.GetName()

		bhp, err := dynClientLocal.Resource(gvrbhp).Namespace(bhpNamespace).Get(ctx, bhpName, metav1.GetOptions{})
		if err == nil {
			annotations := bhp.GetAnnotations()
			if annotations == nil {
				annotations = map[string]string{}
			}
			annotations[byoHostPoolPausedAnnotation] = ""
			bhp.SetAnnotations(annotations)

			_, err = dynClientLocal.Resource(gvrbhp).Namespace(bhpNamespace).Update(ctx, bhp, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating byohostpool on local cluster")
				return err
			}
			log.Info(fmt.Sprintf("byohostpool resource paused : %s  in local cluster", bhpName))

		} else {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, fmt.Sprintf("error while getting byohostpool %s  in local cluster", bhpName))
				return err
			} else {
				log.Info(fmt.Sprintf("no byohostpool %s in local cluster, ignoring", bhpName))
			}
		}
	}

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		lbls := labels.Set{
			bmpv1.KanodBareMetalPoolNameLabel: bmpName,
		}

		bmhList := &bmhv1alpha1.BareMetalHostList{}
		err = r.List(ctx, bmhList, &client.ListOptions{
			Namespace:     bhpScalerNamespace,
			LabelSelector: labels.SelectorFromSet(lbls),
		})
		if err != nil {
			log.Error(err, "Problem while listing baremetalhost resources")
			return err
		}

		for _, bmh := range bmhList.Items {
			bmhName := bmh.Name
			bmhNameSpace := bmh.Namespace
			err = retry.RetryOnConflict(retry.DefaultRetry,
				func() error {
					updatedBmh := &bmhv1alpha1.BareMetalHost{}
					key := client.ObjectKey{Namespace: bmhNameSpace, Name: bmhName}
					if err := r.Client.Get(context.Background(), key, updatedBmh); err != nil {
						log.Error(err, "Failed to get baremetalhost", "name", bmhName, "namespace", bmhNameSpace)
						return err
					}

					annotations := updatedBmh.GetAnnotations()
					if annotations == nil {
						annotations = map[string]string{}
					}

					annotations[bmhPausedAnnotation] = bmhPausedAnnotationKey
					newAnnotation, err := json.Marshal(&bmh.Status)
					if err != nil {
						log.Error(err, "Failed to marshal the BareMetalHost status")
						return err
					}
					obj := map[string]interface{}{}
					if err := json.Unmarshal(newAnnotation, &obj); err != nil {
						log.Error(err, "Failed to unmarshall the BareMetalHost status annotation")
						return err
					}
					delete(obj, "hardware")
					newAnnotation, _ = json.Marshal(obj)
					annotations[bmhv1alpha1.StatusAnnotation] = string(newAnnotation)
					updatedBmh.SetAnnotations(annotations)

					if err = r.Client.Update(context.Background(), updatedBmh); err != nil {
						return err
					}
					log.Info(fmt.Sprintf("baremetalhost %s resource paused and annotation updated with status in local cluster", bmhName))

					return nil
				})

			if err != nil {
				log.Error(err, "Cannot update baremetalhost with new annotation")
				return err
			}
		}
	}
	return nil
}

func (r *ClusterDefReconciler) buildByohContext(
	ctx context.Context,
	log logr.Logger,
	byoHost *byohv1beta1.ByoHost,
	cache *ClusterDefStateCache,
	insecureSkipTLSVerify bool,
) (bool, string, error) {
	var bootstrapKubeconfig = &byohv1beta1.BootstrapKubeconfig{}

	name := fmt.Sprintf("%s-byohpool-kubeconfig", byoHost.Name)
	namespace := byoHost.Namespace

	key := types.NamespacedName{Name: name, Namespace: byoHost.Namespace}

	err := r.Get(ctx, key, bootstrapKubeconfig)
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "cannot get BYOH Kubeconfig in local cluster")
			return true, "", err
		}
	} else {
		log.Info("deleting bootstrapKubeconfig in local cluster", "name", name, "namespace", namespace)
		err := r.Delete(ctx, bootstrapKubeconfig)
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "cannot delete BYOH Kubeconfig in local cluster")
				return true, "", err
			}
		}
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return true, "", err
	}

	gvrbk := schema.GroupVersionResource{
		Group:    "infrastructure.cluster.x-k8s.io",
		Version:  "v1beta1",
		Resource: "bootstrapkubeconfigs",
	}

	bootstrapKubeconfigObject, err := dynClientTarget.Resource(gvrbk).Namespace(namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		if k8serrors.IsNotFound(err) {
			currentContext, presence := cache.Kubeconfig.Contexts[cache.Kubeconfig.CurrentContext]
			if !presence {
				return true, "", errors.New("cannot find current context")
			}
			cluster, presence := cache.Kubeconfig.Clusters[currentContext.Cluster]
			if !presence {
				return true, "", errors.New("cannot find current cluster")
			}

			newBootstrapKubeconfigObject := &unstructured.Unstructured{
				Object: map[string]interface{}{
					"apiVersion": "infrastructure.cluster.x-k8s.io/v1beta1",
					"kind":       "BootstrapKubeconfig",
					"metadata": map[string]interface{}{
						"name":      name,
						"namespace": namespace,
						"annotations": map[string]interface{}{
							"clusterdef.kanod.io/bootstrapkubeconfigenerated": "",
						},
					},
					"spec": map[string]interface{}{
						"apiserver":                  cluster.Server,
						"certificate-authority-data": cluster.CertificateAuthorityData,
						"insecure-skip-tls-verify":   insecureSkipTLSVerify,
					},
				},
			}

			// ownerRef := []metav1.OwnerReference{
			// 	ownedBy(byoHost),
			// }
			// newBootstrapKubeconfigObject.SetOwnerReferences(ownerRef)
			log.Info(fmt.Sprintf("creating bootstrapKubeconfig %s in target cluster", name))

			_, err = dynClientTarget.Resource(gvrbk).Namespace(namespace).Create(ctx, newBootstrapKubeconfigObject, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating bootstrapKubeconfig %s on target cluster", name))
				return true, "", err
			}

			return true, "", nil
		} else {
			log.Error(err, "Cannot get bootstrapKubeconfig with restClient in target cluster")
			return true, "", err
		}
	} else {
		annotation := bootstrapKubeconfigObject.GetAnnotations()
		if _, ok := annotation["clusterdef.kanod.io/bootstrapkubeconfigenerated"]; !ok {
			err := dynClientTarget.Resource(gvrbk).Namespace(namespace).Delete(ctx, name, metav1.DeleteOptions{})
			if err != nil {
				log.Error(err, "Cannot delete existing bootstrapKubeconfig in target cluster")
				return true, "", err
			}
		}

	}

	configData, ok, err := unstructured.NestedString(bootstrapKubeconfigObject.Object, "status", "bootstrapKubeconfigData")
	if err != nil || !ok {
		log.Error(err, "failed to get bootstrapKubeconfigData field in bootstrapKubeconfig status")
		return true, "", nil
	}

	if configData == "" {
		log.Info("bootstrapKubeconfigData field empty in bootstrapKubeconfig status, requeuing")
		return true, "", nil
	}

	log.Info("get BYOH BootstrapKubeconfigData")
	return false, configData, nil
}

// func ownedBy(byoHost *byohv1beta1.ByoHost) metav1.OwnerReference {
// 	return metav1.OwnerReference{
// 		APIVersion: byoHost.APIVersion,
// 		Kind:       byoHost.Kind,
// 		Name:       byoHost.Name,
// 		UID:        byoHost.UID,
// 	}
// }

// Add pause annotation on byohost on local cluster
func (r *ClusterDefReconciler) pauseAndConfigureByoHost(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) (bool, error) {

	namespace := cache.ClusterNamespace

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		lbls := labels.Set{
			bmpv1.KanodBareMetalPoolNameLabel: bmpName,
		}

		byoHostList := &byohv1beta1.ByoHostList{}
		err := r.List(ctx, byoHostList, &client.ListOptions{
			Namespace:     namespace,
			LabelSelector: labels.SelectorFromSet(lbls),
		})
		if err != nil {
			log.Error(err, "Problem while listing byohost resources")
			return false, err
		}

		for _, byoHost := range byoHostList.Items {
			var requeue = false
			var kubeconfig string
			byoHostName := byoHost.Name
			byoHostNameSpace := byoHost.Namespace

			annotations := byoHost.GetAnnotations()
			_, ok := annotations[bootstrapKubeconfigdAnnotation]
			if !ok {
				requeue, kubeconfig, err = r.buildByohContext(ctx, log, &byoHost, cache, false)
				if err != nil {
					return false, err
				}
				if requeue {
					return true, nil
				}

				bootStrapKubeconfigData := base64.StdEncoding.EncodeToString([]byte(kubeconfig))

				err = retry.RetryOnConflict(retry.DefaultRetry,
					func() error {
						updatedByoHost := &byohv1beta1.ByoHost{}
						key := client.ObjectKey{Namespace: byoHostNameSpace, Name: byoHostName}
						if err := r.Client.Get(context.Background(), key, updatedByoHost); err != nil {
							log.Error(err, "Failed to get byohost", "name", byoHostName, "namespace", byoHostNameSpace)
							return err
						}

						annotations := updatedByoHost.GetAnnotations()
						if annotations == nil {
							annotations = map[string]string{}
						}

						annotations[bootstrapKubeconfigdAnnotation] = string(bootStrapKubeconfigData)

						newAnnotation, err := json.Marshal(&updatedByoHost.Status)
						if err != nil {
							log.Error(err, "Failed to marshal the byohost status")
							return err
						}
						annotations[byoHostStatusAnnotation] = string(newAnnotation)
						annotations[capi.PausedAnnotation] = ""

						updatedByoHost.SetAnnotations(annotations)

						if err = r.Client.Update(context.Background(), updatedByoHost); err != nil {
							return err
						}
						log.Info(fmt.Sprintf("byohost %s resource paused and annotation updated with status in local cluster", byoHostName))

						return nil
					})

				if err != nil {
					log.Error(err, "Cannot update byohost with new annotation")
					return false, err
				}
			}
		}
	}

	return false, nil
}

// Add pause annotation on network operator resources on local cluster
func (r *ClusterDefReconciler) pauseNetwork(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvrnetwork := schema.GroupVersionResource{
		Group:    "netpool.kanod.io",
		Version:  "v1",
		Resource: "networks",
	}

	networkNamespace := cache.ClusterNamespace

	for _, networkName := range clusterDef.Spec.PivotInfo.NetworkList {
		network, err := dynClientLocal.Resource(gvrnetwork).Namespace(networkNamespace).Get(ctx, networkName, metav1.GetOptions{})
		if err == nil {
			annotations := network.GetAnnotations()
			annotations[networkAnnotationPaused] = ""
			network.SetAnnotations(annotations)

			// remove network finalizer from network in local cluster
			finalizers := network.GetFinalizers()
			var newfinalizers []string
			for _, finalizerItem := range finalizers {
				if finalizerItem != networkFinalizer {
					newfinalizers = append(newfinalizers, finalizerItem)
				}
			}

			network.SetFinalizers(newfinalizers)

			_, err = dynClientLocal.Resource(gvrnetwork).Namespace(networkNamespace).Update(ctx, network, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating network on local cluster")
				return err
			}
		} else {
			log.Error(err, fmt.Sprintf("error while getting network %s  in local cluster", networkName))
			return err
		}
	}

	return nil
}

// unpause network operator resources on target cluster
func (r *ClusterDefReconciler) unpauseNetworkResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) (error, bool) {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err, false
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvrnetwork := schema.GroupVersionResource{
		Group:    "netpool.kanod.io",
		Version:  "v1",
		Resource: "networks",
	}

	networkNamespace := cache.ClusterNamespace

	for _, networkName := range clusterDef.Spec.PivotInfo.NetworkList {
		// check if network exists on local cluster
		_, err := dynClientLocal.Resource(gvrnetwork).Namespace(networkNamespace).Get(ctx, networkName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting network on local cluster")
				return err, false
			} else {
				networkTarget, err := dynClientTarget.Resource(gvrnetwork).Namespace(networkNamespace).Get(ctx, networkName, metav1.GetOptions{})
				if err != nil {
					log.Error(err, "error while getting network on target cluster")
					return err, false
				}
				annotations := networkTarget.GetAnnotations()
				delete(annotations, networkAnnotationPaused)
				networkTarget.SetAnnotations(annotations)

				_, err = dynClientTarget.Resource(gvrnetwork).Namespace(networkNamespace).Update(ctx, networkTarget, metav1.UpdateOptions{})
				if err != nil {
					log.Error(err, "error while updating network on target cluster")
					return err, false
				}
			}
		} else {
			return nil, false
		}
	}
	return nil, true
}

// Add pause annotation on dhcoconfig resources on local cluster
func (r *ClusterDefReconciler) pauseDhcpconfig(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvrDhcpconfig := schema.GroupVersionResource{
		Group:    "network.kanod.io",
		Version:  "v1alpha1",
		Resource: "dhcpconfigs",
	}
	dhcpConfigNamespace := cache.ClusterNamespace
	for _, dhcpConfigName := range clusterDef.Spec.PivotInfo.DhcpConfigList {
		_, err := dynClientLocal.Resource(gvrDhcpconfig).Namespace(dhcpConfigNamespace).Get(ctx, dhcpConfigName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting dhcpconfig on local cluster")
				return err
			} else {
				log.Info("dhcpconfig missing in local cluster, ignoring", "dhcpConfigName", dhcpConfigName, "dhcpConfigNamespace", dhcpConfigNamespace)
			}
		} else {
			dhcpConfig, err := dynClientLocal.Resource(gvrDhcpconfig).Namespace(dhcpConfigNamespace).Get(ctx, dhcpConfigName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting dhcpconfig on local cluster")
				return err
			}
			annotations := dhcpConfig.GetAnnotations()
			if annotations == nil {
				annotations = map[string]string{}
			}

			annotations[dhcpconfigAnnotationPaused] = ""
			dhcpConfig.SetAnnotations(annotations)

			_, err = dynClientLocal.Resource(gvrDhcpconfig).Namespace(dhcpConfigNamespace).Update(ctx, dhcpConfig, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating dhcpconfig on local cluster")
				return err
			}
		}
	}
	return nil
}

// Add pause annotation on dhcoconfig resources on local cluster
func (r *ClusterDefReconciler) unPauseDhcpconfig(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()
	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrDhcpconfig := schema.GroupVersionResource{
		Group:    "network.kanod.io",
		Version:  "v1alpha1",
		Resource: "dhcpconfigs",
	}

	dhcpConfigNamespace := cache.ClusterNamespace

	for _, dhcpConfigName := range clusterDef.Spec.PivotInfo.DhcpConfigList {
		_, err := dynClientTarget.Resource(gvrDhcpconfig).Namespace(dhcpConfigNamespace).Get(ctx, dhcpConfigName, metav1.GetOptions{})
		if err != nil {
			if !k8serrors.IsNotFound(err) {
				log.Error(err, "error while getting dhcpconfig on target cluster")
				return err
			} else {
				log.Info("dhcpconfig missing in target cluster, ignoring", "dhcpConfigName", dhcpConfigName, "dhcpConfigNamespace", dhcpConfigNamespace)
			}
		} else {
			dhcpConfig, err := dynClientTarget.Resource(gvrDhcpconfig).Namespace(dhcpConfigNamespace).Get(ctx, dhcpConfigName, metav1.GetOptions{})
			if err != nil {
				log.Error(err, "error while getting dhcpconfig on target cluster")
				return err
			}
			annotations := dhcpConfig.GetAnnotations()
			delete(annotations, dhcpconfigAnnotationPaused)
			dhcpConfig.SetAnnotations(annotations)

			_, err = dynClientTarget.Resource(gvrDhcpconfig).Namespace(dhcpConfigNamespace).Update(ctx, dhcpConfig, metav1.UpdateOptions{})
			if err != nil {
				log.Error(err, "error while updating dhcpconfig on target cluster")
				return err
			}

			log.Info("dhcpconfig unpaused", "dhcpConfigName", dhcpConfigName, "dhcpConfigNamespace", dhcpConfigNamespace)

		}
	}
	return nil
}

// Move kanod custom resource
func (r *ClusterDefReconciler) moveKanodResources(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "config.kanod.io",
		Version:  "v1",
		Resource: "kanods",
	}

	gvrcm := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "configmaps",
	}

	// Get kanod custom resource on localcluster
	kanodCrName := clusterDef.Spec.PivotInfo.KanodName
	kanodCrNamespace := clusterDef.Spec.PivotInfo.KanodNamespace
	kanodCustomResource, err := dynClientLocal.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod custom resource on local cluster")
			return err
		} else {
			log.Error(err, fmt.Sprintf("kanod custom resource %s missing in local cluster", kanodCrName))
			return err
		}
	}

	// Get kanod configmap name in custom resource
	kanodConfigName, ok, err := unstructured.NestedString(kanodCustomResource.Object, "spec", "configName")
	if err != nil || !ok {
		log.Error(err, "failed to get configName field in kanod custom resource")
		return err
	}

	// Get kanod configmap in local cluster
	kanodConfig, err := dynClientLocal.Resource(gvrcm).Namespace(clusterDef.Spec.PivotInfo.KanodNamespace).Get(ctx, kanodConfigName, metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod config configmap on local cluster")
			return err
		} else {
			log.Error(err, fmt.Sprintf("kanod config configmap %s missing in local cluster", kanodConfigName))
			return err
		}
	}

	// Create kanod configmap in target cluster
	kanodConfig.SetResourceVersion("")

	_, err = dynClientTarget.Resource(gvrcm).Namespace(clusterDef.Spec.PivotInfo.KanodNamespace).Get(ctx, kanodConfigName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod configmap in target cluster")
			return err
		} else {
			log.Info(fmt.Sprintf("creating kanod configmap %s in target cluster", kanodConfigName))
			_, err = dynClientTarget.Resource(gvrcm).Namespace(clusterDef.Spec.PivotInfo.KanodNamespace).Create(ctx, kanodConfig, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating kanod config configmap %s on target cluster", kanodConfigName))
				return err
			}
		}
	} else {
		log.Info(fmt.Sprintf("kanod configmap %s exists in target cluster\n", kanodConfigName))
	}

	// Create kanod custom resouce in target cluster
	// remove config for argocd and ingres
	// remove ip spec for ironic, interface spec will be specified
	unstructured.RemoveNestedField(kanodCustomResource.Object, "spec", "argocd")
	unstructured.RemoveNestedField(kanodCustomResource.Object, "spec", "ingress")
	unstructured.RemoveNestedField(kanodCustomResource.Object, "spec", "ironic", "ip")

	// update interface for ironic and kanod-kea with the interface of the node on which ironic/kea are deployed
	unstructured.SetNestedField(kanodCustomResource.Object, clusterDef.Spec.PivotInfo.IronicItf, "spec", "ironic", "interface")
	unstructured.SetNestedField(kanodCustomResource.Object, clusterDef.Spec.PivotInfo.IronicItf, "spec", "ironic", "dhcp", "dhcpInterface")

	kanodCustomResource.SetResourceVersion("")
	annotations := kanodCustomResource.GetAnnotations()
	delete(annotations, "operator.kanod.io/progression")
	kanodCustomResource.SetAnnotations(annotations)

	_, err = dynClientTarget.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})
	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while kanod custom resource in target cluster")
			return err
		} else {
			log.Info(fmt.Sprintf("creating kanod custom resource %s in target cluster", kanodCrName))
			_, err = dynClientTarget.Resource(gvr).Namespace(kanodCrNamespace).Create(ctx, kanodCustomResource, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating kanod custom resource %s on target cluster", kanodCrName))
				return err
			}
		}
	} else {
		log.Info(fmt.Sprintf("kanod custom resource %s exists in target cluster\n", kanodCrName))
	}

	return nil
}

func (r *ClusterDefReconciler) isStackDeployed(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger,
) (error, bool) {
	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvr := schema.GroupVersionResource{
		Group:    "config.kanod.io",
		Version:  "v1",
		Resource: "kanods",
	}

	kanodCrName := clusterDef.Spec.PivotInfo.KanodName
	kanodCrNamespace := clusterDef.Spec.PivotInfo.KanodNamespace
	kanodCustomResource, err := dynClientTarget.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod custom resource on target cluster")
			return err, false
		} else {
			log.Error(err, fmt.Sprintf("kanod custom resource %s missing in target cluster", kanodCrName))
			return err, false
		}
	}

	value, ok, err := unstructured.NestedSlice(kanodCustomResource.Object, "status", "conditions")
	if err != nil {
		log.Error(err, "error while getting conditions in kanod custom resource on target cluster")
		return err, false
	}

	if ok {
		for _, conditionsList := range value {
			condition := conditionsList.(map[string]interface{})
			if condition["type"] == "KanodStackReady" && condition["status"] == "True" {
				return nil, true
			}
		}
	} else {
		log.Info("conditions missing in kanod custom resource on target cluster")
		return nil, false
	}

	return nil, false
}

// Install kanod operator on target cluster
func (r *ClusterDefReconciler) installKanodOperator(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	restConfigLocal, _ := rest.InClusterConfig()
	dynClientLocal, err := dynamic.NewForConfig(restConfigLocal)
	if err != nil {
		log.Error(err, "Problem while creating client for local cluster.")
		return err
	}

	gvr := schema.GroupVersionResource{
		Group:    "config.kanod.io",
		Version:  "v1",
		Resource: "kanods",
	}

	kanodCrName := clusterDef.Spec.PivotInfo.KanodName
	kanodCrNamespace := clusterDef.Spec.PivotInfo.KanodNamespace
	kanodCustomResource, err := dynClientLocal.Resource(gvr).Namespace(kanodCrNamespace).Get(ctx, kanodCrName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod custom resource on local cluster")
			return err
		} else {
			log.Error(err, fmt.Sprintf("kanod custom resource %s missing in local cluster", kanodCrName))
			return err
		}
	}

	kanodConfigName, ok, err := unstructured.NestedString(kanodCustomResource.Object, "spec", "configName")
	if err != nil || !ok {
		log.Error(err, "failed to get configName field in kanod custom resource")
		return err
	}

	var config = &corev1.ConfigMap{}
	cmapMeta := client.ObjectKey{
		Name:      kanodConfigName,
		Namespace: kanodCrNamespace,
	}
	err = r.Client.Get(ctx, cmapMeta, config)
	if err != nil {
		r.Log.Error(err, "Cannot find the base config", "configName", kanodConfigName)
		return err
	}
	repository, ok := config.Data["repository"]
	if !ok {
		r.Log.Error(err, "No repository defined in kanod config", "configName", kanodConfigName)
		return err
	}

	registry, ok := config.Data["registry"]
	if !ok {
		r.Log.Error(err, "No registry defined in kanod config", "configName", kanodConfigName)
		return err
	}

	var env = &Env{Env: make(map[string]string)}
	env.Setenv("NEXUS", repository)
	env.Setenv("NEXUS_REGISTRY", registry)

	err = r.ApplyFile(ctx, env, r.Config.KanodOperatorCrd, cache)
	if err != nil {
		r.Log.Error(err, "Cannot perform apply kanod crd.", "url", r.Config.KanodOperatorCrd)
		return err
	}

	return nil
}

func (r *ClusterDefReconciler) isKanodDeployed(
	ctx context.Context,
	cache *ClusterDefStateCache,
	log logr.Logger,
) (error, bool) {
	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err, false
	}

	gvr := schema.GroupVersionResource{
		Group:    "apps",
		Version:  "v1",
		Resource: "deployments",
	}

	kanodDeploymentName := kanodDeploymentName
	kanodDeploymentNamespace := kanodDeploymentNamespace

	kanodDeployment, err := dynClientTarget.Resource(gvr).Namespace(kanodDeploymentNamespace).Get(ctx, kanodDeploymentName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting kanod deployment on target cluster")
			return err, false
		} else {
			log.Error(err, fmt.Sprintf("kanod deployment %s missing in target cluster", kanodDeploymentName))
			return err, false
		}
	}

	value, ok, err := unstructured.NestedSlice(kanodDeployment.Object, "status", "conditions")
	if err != nil {
		log.Error(err, "error while getting conditions in kanod deployment on target cluster")
		return err, false
	}

	if ok {
		for _, conditionsList := range value {
			condition := conditionsList.(map[string]interface{})
			if condition["type"] == string(appsv1.DeploymentAvailable) && condition["status"] == string(corev1.ConditionTrue) {
				return nil, true
			}
		}
	} else {
		log.Info("conditions missing in kanod deployment on target cluster")
		return nil, false
	}

	return nil, false
}

func (r *ClusterDefReconciler) createNamespace(
	ctx context.Context,
	cache *ClusterDefStateCache,
	nsName string,
	log logr.Logger,
) error {

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)
	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	gvrns := schema.GroupVersionResource{
		Group:    "",
		Version:  "v1",
		Resource: "namespaces",
	}

	nsSpec := &unstructured.Unstructured{
		Object: map[string]interface{}{
			"apiVersion": "v1",
			"kind":       "Namespace",
			"metadata": map[string]interface{}{
				"name": nsName,
			},
		},
	}

	_, err = dynClientTarget.Resource(gvrns).Get(ctx, nsName, metav1.GetOptions{})

	if err != nil {
		if !k8serrors.IsNotFound(err) {
			log.Error(err, "error while getting namespace on target cluster")
			return err
		} else {
			log.Info(fmt.Sprintf("creating namespace %s on target cluster", nsName))
			_, err = dynClientTarget.Resource(gvrns).Create(ctx, nsSpec, metav1.CreateOptions{})
			if err != nil {
				log.Error(err, fmt.Sprintf("error while creating namespace %s on target cluster", nsName))
				return err
			}
		}
	} else {
		log.Info(fmt.Sprintf("namespace %s exists in target cluster\n", nsName))
	}

	return nil
}

func (r *ClusterDefReconciler) pauseCapi(ctx context.Context, clusterDef *gitopsv1.ClusterDef) error {
	pausePatch := `{"spec":{"paused": true}}`
	err := r.Client.Patch(
		ctx,
		&capi.Cluster{
			ObjectMeta: metav1.ObjectMeta{
				Name:      clusterDef.Name,
				Namespace: r.getTargetClusterNamespace(clusterDef),
			},
		},
		client.RawPatch(types.MergePatchType, []byte(pausePatch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot patch clusterapi.")
	}

	return err
}

func (r *ClusterDefReconciler) scaleLocalIronicDeployment(ctx context.Context, replicasValue int32) error {
	patch := fmt.Sprintf(`{"spec":{"replicas": %d}}`, replicasValue)

	err := r.Client.Patch(
		ctx,
		&appsv1.Deployment{
			ObjectMeta: metav1.ObjectMeta{
				Name:      ironicDeploymentName,
				Namespace: ironicDeploymentNamespace,
			},
		},
		client.RawPatch(types.MergePatchType, []byte(patch)),
	)
	if err != nil {
		r.Log.Error(err, "Cannot scale baremetal-operator-ironic deployment.")
	}

	return err
}

func (r *ClusterDefReconciler) isIronicDeploymentReady(
	ctx context.Context,
	log logr.Logger,
) (error, bool) {
	var ironicDeployment appsv1.Deployment

	err := r.Client.Get(
		ctx,
		types.NamespacedName{
			Name:      ironicDeploymentName,
			Namespace: ironicDeploymentNamespace,
		},
		&ironicDeployment,
	)

	if err != nil {
		log.Error(err, "Cannot get ironic deployment on local cluster.")
		return err, false
	}

	for _, condition := range ironicDeployment.Status.Conditions {
		if condition.Type == appsv1.DeploymentAvailable && condition.Status == corev1.ConditionTrue {
			return nil, true
		}
	}

	return nil, false
}

// Add pause annotation on baremetalhost on local cluster
func (r *ClusterDefReconciler) pauseBareMetalHost(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	bmhNamespace := cache.ClusterNamespace

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		lbls := labels.Set{
			bmpv1.KanodBareMetalPoolNameLabel: bmpName,
		}

		bmhList := &bmhv1alpha1.BareMetalHostList{}
		err := r.List(ctx, bmhList, &client.ListOptions{
			Namespace:     bmhNamespace,
			LabelSelector: labels.SelectorFromSet(lbls),
		})
		if err != nil {
			log.Error(err, "Problem while listing baremetalhost resources")
			return err
		}

		for _, bmh := range bmhList.Items {
			bmhName := bmh.Name
			bmhNameSpace := bmh.Namespace
			err = retry.RetryOnConflict(retry.DefaultRetry,
				func() error {
					updatedBmh := &bmhv1alpha1.BareMetalHost{}
					key := client.ObjectKey{Namespace: bmhNameSpace, Name: bmhName}
					if err := r.Client.Get(context.Background(), key, updatedBmh); err != nil {
						log.Error(err, "Failed to get baremetalhost", "name", bmhName, "namespace", bmhNameSpace)
						return err
					}

					annotations := updatedBmh.GetAnnotations()
					if annotations == nil {
						annotations = map[string]string{}
					}

					// bmh is already paused as the cluster is paused
					// the annotation key is changed in order to keep control on the bmh unpause process
					// for bmh not in provisionned state
					if updatedBmh.Status.Provisioning.State != bmhv1alpha1.StateProvisioned {
						annotations[bmhPausedAnnotation] = bmhPausedAnnotationKanodKey
						updatedBmh.SetAnnotations(annotations)
						if err = r.Client.Update(context.Background(), updatedBmh); err != nil {
							return err
						}
						log.Info(fmt.Sprintf("baremetalhost %s resource paused with kanod ney", bmhName))
					}

					return nil
				})

			if err != nil {
				log.Error(err, "Cannot update baremetalhost with new annotation")
				return err
			}
		}
	}
	return nil
}

// unpause byohostpoolscaler resources on target cluster
func (r *ClusterDefReconciler) unpauseBareMetalHost(
	ctx context.Context,
	clusterDef *gitopsv1.ClusterDef,
	cache *ClusterDefStateCache,
	log logr.Logger) error {

	clientConfig := clientcmd.NewDefaultClientConfig(
		*cache.Kubeconfig,
		&clientcmd.ConfigOverrides{},
	)

	restConfigTarget, _ := clientConfig.ClientConfig()

	dynClientTarget, err := dynamic.NewForConfig(restConfigTarget)
	if err != nil {
		log.Error(err, "Problem while creating client for target cluster.")
		return err
	}

	bmhNamespace := cache.ClusterNamespace

	gvrbmh := schema.GroupVersionResource{
		Group:    "metal3.io",
		Version:  "v1alpha1",
		Resource: "baremetalhosts",
	}

	for _, bmpName := range clusterDef.Spec.PivotInfo.BareMetalPoolList {
		labelSelector := metav1.LabelSelector{MatchLabels: map[string]string{bmpv1.KanodBareMetalPoolNameLabel: bmpName}}

		options := metav1.ListOptions{
			LabelSelector: labels.Set(labelSelector.MatchLabels).String(),
		}
		bmhRsc, err := dynClientTarget.Resource(gvrbmh).Namespace(bmhNamespace).List(ctx, options)
		if err != nil {
			log.Error(err, "Problem while listing baremetalhosts resources in target cluster")
			return err
		}

		for _, crd := range bmhRsc.Items {
			bmhName := crd.GetName()
			bmhNamespace := crd.GetNamespace()

			bmh, err := dynClientTarget.Resource(gvrbmh).Namespace(bmhNamespace).Get(ctx, bmhName, metav1.GetOptions{})
			if err == nil {
				annotations := bmh.GetAnnotations()
				val, ok := annotations[bmhPausedAnnotation]

				// delete pause annotation with kanod key
				if ok && val == bmhPausedAnnotationKanodKey {
					delete(annotations, bmhPausedAnnotation)
					bmh.SetAnnotations(annotations)

					_, err = dynClientTarget.Resource(gvrbmh).Namespace(bmhNamespace).Update(ctx, bmh, metav1.UpdateOptions{})
					if err != nil {
						log.Error(err, "error while updating baremetalhost in target cluster")
						return err
					}
					log.Info(fmt.Sprintf("pause annotation (with kanod key) removed from baremetalhost resource %s  in target cluster", bmhName))
				}
			} else {
				if !k8serrors.IsNotFound(err) {
					log.Error(err, fmt.Sprintf("error while getting baremetalhost %s  in target cluster", bmhName))
					return err
				} else {
					log.Info(fmt.Sprintf("no baremetalhost %s in target cluster, ignoring", bmhName))
				}
			}
		}
	}

	return nil
}
